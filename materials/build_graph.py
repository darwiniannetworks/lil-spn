import networkx as nx


def _create_sum_children(G,old_children,confg):
    new_children = []
    for _ in range(confg["num_sums"]):
        confg["node_id"] += 1
        new_children.append(confg["node_id"])
        G.add_node(confg["node_id"],type="sum", label="+{}".format(confg["node_id"]))
        for left_child in old_children:
            G.add_edge(confg["node_id"],left_child)
    return new_children

def _create_product_children(G,children_partition_1,children_partition_2,confg):
    product_node_ids = []
    product_conns = [(i,j) for i in children_partition_1 for j in children_partition_2 if i!=j]
    for product_conn in product_conns:
        confg["node_id"] += 1
        product_node_ids.append(confg["node_id"])
        G.add_node(confg["node_id"],type="product",label="*{}".format(confg["node_id"]))
        G.add_edge(confg["node_id"],product_conn[0])
        G.add_edge(confg["node_id"],product_conn[1])
    return product_node_ids

def _chop_or_slice(indexes_1, indexes_2, G, confg):
    product_node_ids = []
    for i in range(1,len(indexes_1)):

        partition_1 = indexes_1[:i]
        partition_1_children = build_graph( indexes_2, partition_1, G, confg )
        if len(partition_1) > 1 or len(indexes_2) > 1:
            partition_1_children = _create_sum_children(G,partition_1_children,confg)
        
        partition_2 = indexes_1[i:]
        partition_2_children = build_graph( indexes_2, partition_2, G, confg )
        if len(partition_2) > 1 or len(indexes_2) > 1:
            partition_2_children = _create_sum_children(G,partition_2_children,confg)

        product_node_ids = product_node_ids + _create_product_children(G,partition_1_children,partition_2_children,confg)

    return product_node_ids

node_id = -1

def build_graph(M,N,G,confg):

    # Vertical chops
    if len(N) > 1:
        return _chop_or_slice(N,M,G,confg)
        # product_node_ids = []
        # for i in range(1,len(N)):

        #     left_N = N[:i]
        #     left_children = build_graph( M, left_N, G, confg )
        #     if len(left_N) > 1 or len(M) > 1:
        #         left_children = _create_sum_children(G,left_children,confg)
            
        #     right_N = N[i:]
        #     right_children = build_graph( M, right_N, G, confg )
        #     if len(right_N) > 1 or len(M) > 1:
        #         right_children = _create_sum_children(G,right_children,confg)

        #     product_node_ids = product_node_ids + _create_product_children(G,left_children,right_children,confg)

        # return product_node_ids
    
    # Horizontal slices
    elif len(M) > 1:
        return _chop_or_slice(M,N,G,confg)
        # product_node_ids = []
        # for i in range(1,len(M)):

        #     left_M = M[:i]
        #     left_children = build_graph( left_M, N, G, confg )
        #     if len(left_M) > 1 or len(N) > 1:
        #         left_children = _create_sum_children(G,left_children,confg)
            
        #     right_M = M[i:]
        #     right_children = build_graph( right_M, N, G, confg )
        #     if len(right_M) > 1 or len(N) > 1:
        #         right_children = _create_sum_children(G,right_children,confg)

        #     product_node_ids = product_node_ids + _create_product_children(G,left_children,right_children,confg)

        # return product_node_ids

    # Leaf
    else:
        leaf_node_ids = []
        for _ in range(confg["num_sums"]):
            confg["node_id"] += 1
            leaf_node_ids.append(confg["node_id"])
            G.add_node(confg["node_id"],type="leaf", scope=(M[0],N[0]), label="({},{})".format(M[0],N[0]))
        return leaf_node_ids
        

if __name__ == "__main__":
    
    G = nx.DiGraph()

    confg_dict = {"num_sums": 1, "node_id": -1}
    root_children = []
    root_children = root_children + build_graph(["L"+str(n) for n in range(8)],["C"+str(n) for n in range(8)],G,confg_dict)
    root_children = root_children + build_graph(["C"+str(n) for n in range(8)],["L"+str(n) for n in range(8)],G,confg_dict)
    confg_dict["node_id"] += 1
    G.add_node(confg_dict["node_id"],type="sum", label="+{}".format(confg_dict["node_id"]))
    for root_child in root_children:
        G.add_edge(confg_dict["node_id"],root_child)

    nx.drawing.nx_pydot.write_dot(G, "G.dot")
