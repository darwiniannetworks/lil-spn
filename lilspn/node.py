import tensorflow as tf
from lilspn.utilities import EPSILON


MARG_VAR_VAL = -1


class Node:
    '''
    '''
    node_id = -1

    def __init__(self):
        
        self.children = []
        Node.node_id += 1
        self.id = Node.node_id

        self.reset_state()


    def reset_state(self):
        self.value = None
        self.mpe_mask = None
        self.mpe_value = None
        self.is_built = False


    def compute_value(self, children_values):
        
        raise NotImplementedError


    def compute_children_mpe_mask(self):
        # Returns shape (N x V), where V is the number of children
        
        raise NotImplementedError


    def compute_mpe_value(self):

        raise NotImplementedError


    def build(self):
        if not self.is_built:
            self.build_node()
            self.is_built = True


    def build_node(self):
        pass




class SumNode(Node):
    '''
    '''

    def __init__(self, *nodes):

        if len(nodes) < 1:
            raise ValueError("Sum Node must have children")

        # call parent construction before
        super().__init__()
        
        self.children = list(nodes)
        self.linear_value = None


    def build_node(self):
        self.weights = tf.get_variable(
            "weights_{}".format(self.id),
            shape=(1,len(self.children)),
            initializer=tf.initializers.random_uniform(minval=0, maxval=1)
            # initializer=tf.glorot_normal_initializer()
            ) # weight shape --> (1 x V)
        tf.summary.scalar('mean', tf.reduce_mean(self.weights))


    def _prepare_weights(self):
        non_neg_weights = tf.add(tf.nn.relu(tf.subtract(self.weights, EPSILON)), EPSILON)
        normalized_weights = tf.divide(non_neg_weights,tf.reduce_sum(non_neg_weights))
        return normalized_weights


    def _compute_linear_value(self, children_values):
        # Calculate linear part of the value of a node
        # Useful for forward pass and MPE

        normalized_weights = self._prepare_weights()

        log_weights = tf.log(normalized_weights)

        log_mult = tf.add(log_weights,children_values)              ## resulting shape (N x V)

        return log_mult


    def compute_value(self, children_values):

        with tf.name_scope("Sum_Node_value_{}".format(self.id)):

            self.linear_value = self._compute_linear_value(children_values)          ## resulting shape (N x V)

            max_log_mult = tf.reduce_max(self.linear_value, axis=1, keepdims=True)   ## resulting shape (N x 1)

            log_mult_shift = tf.subtract(self.linear_value,max_log_mult)             ## resulting shape (N x V)

            exp_mult = tf.exp(log_mult_shift)                                        ## shape (N x V)

            node_value_shift = tf.reduce_sum(exp_mult, axis=1, keepdims=True)        ## resulting shape (N x 1)

            log_node_value_shift = tf.log(node_value_shift)                          ## shape (N x 1)

            node_value = tf.add(log_node_value_shift, max_log_mult)                  ## shape (N x 1)

            return node_value


    def compute_children_mpe_mask(self):

        with tf.name_scope("Sum_Node_MPE_mask_{}".format(self.id)):

            children_pos_idx = tf.argmax(self.linear_value, axis=1) # Shape (N x )
            mpe_mask_out = tf.one_hot(children_pos_idx, depth=len(self.children)) # Shape (N x V)

            return mpe_mask_out




class ProductNode(Node):

    def __init__(self, *nodes):

        if len(nodes) < 1:
            raise ValueError("Product Node must have children")

        # call parent construction before
        super().__init__()
        
        self.children = list(nodes)
        
    def compute_value(self, children_values):

        with tf.name_scope("Product_Node_value_{}".format(self.id)):

            node_value = tf.reduce_sum(children_values, axis=1, keepdims=True)

            return node_value

    def compute_children_mpe_mask(self):

        with tf.name_scope("Product_Node_MPE_mask_{}".format(self.id)):

            mpe_mask_out = tf.ones(shape=(tf.shape(self.value)[0],len(self.children)), dtype=tf.float32)

            return mpe_mask_out




class LeafNode(Node):

    def __init__(self, index_ph):
        # The index in the input placeholder works as this leaf scope
        super().__init__()

        self.scope = index_ph
        self.leaf_input = None

    def set_input(self, placeholder):
        # Assuming image (batch, height, width) resulting shape ( N x 1)
        self.leaf_input = tf.reshape(placeholder[:,self.scope[0],self.scope[1]],shape=[-1,1])


    def compute_value(self, children_values):
        with tf.name_scope("Leaf_Node_value_{}".format(self.id)):
            leaf_value = self.compute_leaf_value()
            final_value = tf.where(tf.equal(self.leaf_input, MARG_VAR_VAL), tf.ones(tf.shape(self.leaf_input)), leaf_value)
            return tf.log(final_value)


    def compute_mpe_value(self):
        with tf.name_scope("Leaf_Node_MPE_value_{}".format(self.id)):
            leaf_mpe_value = self.compute_leaf_mpe_value()
            leaf_mpe_value = tf.tile(leaf_mpe_value,multiples=[tf.shape(self.leaf_input)[0],1])
            final_mpe_value = tf.where(tf.equal(self.leaf_input, MARG_VAR_VAL), leaf_mpe_value, self.leaf_input)
            return final_mpe_value


    def compute_leaf_mpe_value(self):
        raise NotImplementedError


    def compute_leaf_value(self):
        raise NotImplementedError



class GaussianLeafNode(LeafNode):

    def __init__(self, index_ph, mean, std):

        # call parent construction before
        super().__init__(index_ph)

        self.mean_value = mean
        self.std_value = std


    def build_node(self):

        self.mean = tf.get_variable("mean_{}".format(self.id), initializer=tf.constant(self.mean_value, shape=(1,1), dtype=tf.float32), dtype=tf.float32, trainable=False)
        self.std = tf.get_variable("std_{}".format(self.id), initializer=tf.constant(self.std_value, shape=(1,1), dtype=tf.float32), dtype=tf.float32, trainable=False)
        self.normal = tf.distributions.Normal(loc=self.mean, scale=self.std, name="normal_{}".format(self.id))


    def compute_leaf_value(self):
        with tf.name_scope("Gaussian_Leaf_Node_value_{}".format(self.id)):
            return self.normal.prob(self.leaf_input)


    def compute_leaf_mpe_value(self):
        with tf.name_scope("Gaussian_Leaf_MPE_value_{}".format(self.id)):
            return self.mean




class InputLeafNode(LeafNode):


    def __init__(self, index_ph):

        # call parent construction before
        super().__init__(index_ph)


    def compute_leaf_value(self):

        return self.leaf_input


    def compute_leaf_mpe_value(self):

        return self.leaf_input



