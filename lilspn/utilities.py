import numpy as np
import math

EPSILON = 1e-3		 ## Small enough close to zero
LOG_ZERO = -1e-3 	 ## Because 10e(-1e-3) = 0 in Python




class Normalizer:

    def normalize(self, data, degree_of_freedom = 0):
        '''
        Normalizing errors when population parameters are known. Works well for populations that are ***normally distributed***

        The data will have zero mean and unit std

        The data can contain values outside the range(0,1)
        '''
        self.global_mean = np.mean(data, axis=0)
        self.global_std = np.std(data,axis=0,ddof=degree_of_freedom)

        self.std_mask = self.global_std==0
        self.global_std[self.std_mask] = 1
        # This means that every data value is equal to the mean. This result along with the one above allows us to say 
        # that the sample standard deviation of a data set is zero if and only if all of its values are identical.

        
        # Zero mean and unit std
        norm_data = (data - self.global_mean) / self.global_std
        
        return norm_data


    def de_normalize(self, data):
        self.global_std[self.std_mask] = 0
        return data * self.global_std + self.global_mean




def random_mini_batches(X, Y=None, mini_batch_size=64, seed=0):
    """
    Creates a list of random minibatches from (X, Y)
    
    Arguments:
    X -- input data, of shape (input size, number of examples) (m, Hi, Wi)
    mini_batch_size - size of the mini-batches, integer
    seed -- this is only for the purpose of grading, so that you're "random minibatches are the same as ours.
    
    Returns:
    mini_batches -- list of synchronous (mini_batch_X)
    """

    m = X.shape[0]                  # number of training examples
    mini_batches = []
    np.random.seed(seed)

    # Step 1: Shuffle (X, Y)
    permutation = list(np.random.permutation(m))
    shuffled_X = X[permutation, :, :]
    shuffled_Y = None
    if Y:
        shuffled_Y = Y[permutation, :, :]

    # Step 2: Partition (shuffled_X, shuffled_Y). Minus the end case.
    # number of mini batches of size mini_batch_size in your partitionning
    num_complete_minibatches = math.floor(m / mini_batch_size)
    for k in range(0, num_complete_minibatches):
        mini_batch_X = shuffled_X[k * mini_batch_size: k * mini_batch_size + mini_batch_size, :, :]
        mini_batch = None
        if Y:
            mini_batch_Y = shuffled_Y[k * mini_batch_size: k * mini_batch_size + mini_batch_size, :, :]
            mini_batch = (mini_batch_X, mini_batch_Y)
        else:
            mini_batch = (mini_batch_X, None)
        mini_batches.append(mini_batch)

    # Handling the end case (last mini-batch < mini_batch_size)
    if m % mini_batch_size != 0:
        mini_batch_X = shuffled_X[num_complete_minibatches * mini_batch_size: m, :, :]
        mini_batch = None
        if Y:
            mini_batch_Y = shuffled_Y[num_complete_minibatches * mini_batch_size: m, :, :]
            mini_batch = (mini_batch_X, mini_batch_Y)
        else:
            mini_batch = (mini_batch_X, None)
        mini_batches.append(mini_batch)

    return mini_batches



class DataRectangle:
        """
        A little util class for storing the sets of indexes for the rows and columns considered
        """
        class_counter = 0

        @classmethod
        def reset_id_counter(cls):
            """
            WRITEME
            """
            DataRectangle.class_counter = 0

        @classmethod
        def whole_slice(cls,
                        n_rows,
                        n_columns):

            rows = np.arange(n_rows, dtype=np.uint32)
            columns = np.arange(n_columns, dtype=np.uint32)
            return DataRectangle(rows, columns)

        def __init__(self,
                     rows=None,
                     columns=None):
            self.rows_ids = np.array(rows)
            self.columns_ids = np.array(columns)
            #to hold the sum nodes ids that this rectangle is attached to
            self.sum_nodes_ids = np.array([-1])
            self.id = DataRectangle.class_counter
            DataRectangle.class_counter += 1

        def __hash__(self):
            return hash(self.id)
        
        def n_rows(self):
            return len(self.rows_ids)

        def n_columns(self):
            return len(self.columns_ids)

        def set_sum_nodes_ids(self,sum_nodes_numpy):
            self.sum_nodes_ids = sum_nodes_numpy

        def __repr__(self):
            return ("[id: {id} m :{rows} n :{columns}\n]".
                    format(id=self.id,
                           rows=self.rows_ids,
                           columns=self.columns_ids))
   
class Region():
    """
        Using the coordinates of the four corners
        as the identifier for a specific region of
        the picture.
    """
    __region_dict = {}

    def __init__(self, id_num, _ru, _rd, _cl, _cr,_iw ,_ih, _k_w, _k_h):
        #to avoid using another file like the Parameters
        self.imageWidth = _iw
        self.imageHeight = _ih
        self.baseResolution_w = _k_w
        self.baseResolution_h = _k_h

        self.id = id_num
        self.rowUp = _ru
        self.rowDown = _rd
        self.columnLeft = _cl
        self.columnRight = _cr
        # the size statistics of the region
        self.width = _cr - _cl
        self.height = _rd - _ru
        assert self.width <= self.imageWidth
        assert self.height <= self.imageHeight
        
        if self.width <= self.baseResolution_w and self.height <= self.baseResolution_h:
            self.interval_w = 1
            self.interval_h = 1
            self.fine_or_coarse = 'fine'
        else:
            self.interval_w = _k_w
            self.interval_h = _k_h
            self.fine_or_coarse = 'coarse'
        
        # SumNodes for the region
        self.sumNodes = []
        self.from_combination = (0,0)

    def __str__(self):
        return 'Region : <%d,%d,%d,%d>' % \
              (self.columnLeft, self.columnRight, self.rowUp, self.rowDown)
    
    def _cuts_selection(self, _vertical_cuts, _horizontal_cuts,_type_cuts_selection):
        type_selection = _type_cuts_selection
        
        #all possible vertical/horizontal cuts
        all_vertical_cuts = _vertical_cuts
        all_horizontal_cuts = _horizontal_cuts
        
        #type of selection cuts
        if type_selection == 'central1':
            if len(all_vertical_cuts) != 0:#vertical
                
                if len(all_vertical_cuts)%2 == 0:#even
                    central = int((len(all_vertical_cuts)-2)/2)
                    vertical_cuts = [all_vertical_cuts[central], all_vertical_cuts[1+central]]
                else:#odd
                    central = int((len(all_vertical_cuts)-1)/2)
                    vertical_cuts = [all_vertical_cuts[central]]
            else:
                vertical_cuts=all_vertical_cuts
                
            if len(all_horizontal_cuts) != 0:#horizontal
                
                if len(all_horizontal_cuts)%2 == 0:#even
                    central = int((len(all_horizontal_cuts)-2)/2)
                    horizontal_cuts = [all_horizontal_cuts[central], all_horizontal_cuts[1+central]]
                else:#odd
                    central = int((len(all_horizontal_cuts)-1)/2)
                    horizontal_cuts = [all_horizontal_cuts[central]]
            else:
                horizontal_cuts=all_horizontal_cuts
        
        elif type_selection == 'central':
            if len(all_vertical_cuts) != 0:#vertical
                
                if len(all_vertical_cuts)%2 == 0:#even
                    central = int((len(all_vertical_cuts)-2)/2)
                    vertical_cuts = [all_vertical_cuts[central]]
                else:#odd
                    central = int((len(all_vertical_cuts)-1)/2)
                    vertical_cuts = [all_vertical_cuts[central]]
            else:
                vertical_cuts=all_vertical_cuts
                
            if len(all_horizontal_cuts) != 0:#horizontal
                
                if len(all_horizontal_cuts)%2 == 0:#even
                    central = int((len(all_horizontal_cuts)-2)/2)
                    horizontal_cuts = [all_horizontal_cuts[central]]
                else:#odd
                    central = int((len(all_horizontal_cuts)-1)/2)
                    horizontal_cuts = [all_horizontal_cuts[central]]
            else:
                horizontal_cuts=all_horizontal_cuts
        
        elif type_selection == 'random':
            #select ONE randomly among all vertical and horizontal cuts]
            all_cuts = all_vertical_cuts + all_horizontal_cuts
            index = np.random.randint(0,len(all_cuts))
            selected_cut = [all_cuts[index]]
            if index < len(all_vertical_cuts):#the selected cut is from vertical
                vertical_cuts = selected_cut
                horizontal_cuts = []
            else:
                vertical_cuts = []
                horizontal_cuts = selected_cut

        elif type_selection == 'vert':
            #selecting ONE cut among all vertical and horizontal cuts: always the first one in the list
            if len(all_vertical_cuts) != 0:#                vertical cut has priority
                vertical_cuts = [all_vertical_cuts[0]]
                horizontal_cuts=[]
            else:
                vertical_cuts=[]
                if len(all_horizontal_cuts) != 0:#horizontal
                    horizontal_cuts = [all_horizontal_cuts[0]]
                else:
                    horizontal_cuts=[]
        else:
            #------------------ Selection of all possible cuts
            vertical_cuts = all_vertical_cuts
            horizontal_cuts = all_horizontal_cuts

        return vertical_cuts, horizontal_cuts

    def cut_region(self, graph,_global_nodes_id,_type_cuts_selection):
        
        g = graph
        global_nodes_id = _global_nodes_id
        
        cl = self.columnLeft
        cr = self.columnRight
        ru = self.rowUp
        rd = self.rowDown
        step_w = self.interval_w
        step_h = self.interval_h
        
        vertical_cuts = [n for n in range(cl + step_w, cr, step_w)]
        horizontal_cuts = [n for n in range(ru + step_h, rd, step_h)]
        #function to select which cuts will be done
        if _type_cuts_selection != 'all':
            vertical_cuts, horizontal_cuts = self._cuts_selection(vertical_cuts, horizontal_cuts,_type_cuts_selection) 

        g.add_nodes_from(self.sumNodes,type='sum',label='regionId:{}+{}:{}_id({})ru{}rd{}cl{}cr{}'.format(self.id,self.from_combination,self.fine_or_coarse,self.id,self.rowUp,self.rowDown,self.columnLeft,self.columnRight))#these are sum nodes

        #Vertical cuts: try to decompose into left and right parts
        for index in vertical_cuts:
            
            lr_id = Region.getRegionId(ru, rd, cl, index, self.imageWidth, self.imageHeight, self.baseResolution_w, self.baseResolution_h)
            rr_id = Region.getRegionId(ru, rd, index, cr, self.imageWidth, self.imageHeight, self.baseResolution_w, self.baseResolution_h)
            #getting left and right regions
            left_rec = Region.getRegion(lr_id) 
            left_sum_nodes_ids = left_rec.sumNodes
            right_rec = Region.getRegion(rr_id)
            right_sum_nodes_ids = right_rec.sumNodes
            
            # The prod nodes(the number of product nodes are the amount necessary to link the children regions nodes combinations)
            n_prod = len(left_sum_nodes_ids)*len(right_sum_nodes_ids)#number of prod nodes added per cut
            prod_nodes_ids = np.arange(global_nodes_id[0],global_nodes_id[0]+n_prod)
            #pointing global to the last id created
            global_nodes_id[0] += n_prod
            #creating prod nodes in graph
            g.add_nodes_from(prod_nodes_ids,type='product',label='cut_regionId:{}*V{}'.format(self.id,index))
                                                            #-------------adding the edges------------
            #from each of the current region sum_nodes_ids to each of the product nodes ids
            #g.add_nodes_from(self.sumNodes,type='sum',label='regionId:{}+{}:{}_id({})ru{}rd{}cl{}cr{}'.format(self.id,self.from_combination,self.fine_or_coarse,self.id,self.rowUp,self.rowDown,self.columnLeft,self.columnRight))#these are sum nodes
            for i in self.sumNodes:
                for j in prod_nodes_ids:
                    g.add_edge(i,j)
                    
            #each product id gets one combination of left_ids and right_ids regions
            comb = [(x,y) for x in left_sum_nodes_ids for y in right_sum_nodes_ids]
            for i,c in zip(prod_nodes_ids,comb):
                    g.add_edge(i,c[0])
                    g.add_edge(i,c[1])
                    
        #Horizontal cuts: try to decompose into up and down parts
        for index in horizontal_cuts:
            
            ur_id = Region.getRegionId(ru, index, cl, cr, self.imageWidth, self.imageHeight, self.baseResolution_w, self.baseResolution_h)
            dr_id = Region.getRegionId(index, rd, cl, cr, self.imageWidth, self.imageHeight, self.baseResolution_w, self.baseResolution_h)
            #getting upper and botton regions
            upper_rec = Region.getRegion(ur_id)
            left_sum_nodes_ids = upper_rec.sumNodes
            bottom_rec = Region.getRegion(dr_id)
            right_sum_nodes_ids = bottom_rec.sumNodes

            # The prod nodes(the number of product nodes are the amount necessary to link the children regions nodes combinations)
            n_prod = len(left_sum_nodes_ids)*len(right_sum_nodes_ids)
            prod_nodes_ids = np.arange(global_nodes_id[0],global_nodes_id[0]+n_prod)
            #pointing global to the last id created
            global_nodes_id[0] += n_prod
            #creating prod nodes in graph
            g.add_nodes_from(prod_nodes_ids,type='product',label='cut_regionId:{}*H{}'.format(self.id,index))
                                                            #-------------adding the edges------------
            #from each of the current region sum_nodes_ids to each of the product nodes ids
            #g.add_nodes_from(self.sumNodes,type='sum',label='regionId:{}+{}:{}_id({})ru{}rd{}cl{}cr{}'.format(self.id,self.from_combination,self.fine_or_coarse,self.id,self.rowUp,self.rowDown,self.columnLeft,self.columnRight))#these are sum nodes
            for i in self.sumNodes:
                for j in prod_nodes_ids:
                    g.add_edge(i,j)
                    
            #each product id gets one combination of left_ids and right_ids
            comb = [(x,y) for x in left_sum_nodes_ids for y in right_sum_nodes_ids]
            for i,c in zip(prod_nodes_ids,comb):
                    g.add_edge(i,c[0])
                    g.add_edge(i,c[1])
                    
    @staticmethod
    def getRegionId(_rowUp, _rowDown, _columnLeft, _columnRight, _iw, _ih, _k_w, _k_h):
        #using the cantor pairing function 3 times to encode the 4 numbers in 1
        first = 0.5*(_rowUp+_rowDown)*(_rowUp+_rowDown+1)+_rowDown
        second = 0.5*(first+_columnLeft)*(first+_columnLeft+1)+_columnLeft
        id_num = 0.5*(second+_columnRight)*(second+_columnRight+1)+_columnRight
        if id_num - int(id_num) != 0.0:
            print('----------WARNING: FLOAT NUMBER AS ID')#it's not supposed to happen(cantor definition)
        id_num = int(id_num)
        #if region doesn' exist, create one
        if id_num not in Region.__region_dict:
            Region.__region_dict[id_num] = Region(id_num, _rowUp, _rowDown, _columnLeft, _columnRight, _iw, _ih, _k_w, _k_h)
        return id_num

    @staticmethod
    def getRegion(id_num):
        return Region.__region_dict[id_num]

    @staticmethod
    def reset_regions_dict():
        Region.__region_dict = {}
    
    @staticmethod
    def get_regions_dict():
        return Region.__region_dict

