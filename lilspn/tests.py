import unittest
import tensorflow as tf
import numpy as np
import networkx as nx
from collections import deque

from lilspn.spn import SumProductNetwork
from lilspn.node import SumNode, ProductNode, InputLeafNode, LeafNode, GaussianLeafNode
from lilspn.learning import StructuralLearning, Conv2DStructuralLearning, PoonAndDomingosStructuralLearning, PercentileGaussianLeafLearning, LeafLearning

from scipy.stats import norm

#[TODO: remove]
import time
class TicTac:

    def __init__(self):
        self.timers = {}
        self.timers_ids_stack = []

    def tic(self):
        if len(self.timers_ids_stack) != 0:
            new_id = 1 + self.timers_ids_stack[len(self.timers_ids_stack)-1]
            self.timers_ids_stack.append(new_id)
            self.timers[new_id] = [time.perf_counter()]
        else:
            self.timers_ids_stack.append(0)
            self.timers[0] = [time.perf_counter()]
    
    def toc(self):
        if len(self.timers_ids_stack) != 0:
            current_id = self.timers_ids_stack.pop()
            self.timers[current_id].append(time.perf_counter())
            return (self.timers[current_id][1] - self.timers[current_id][0])
        #else:  #no timers started!!

class SumNodeTest(unittest.TestCase):


    def test_compute_value_1D(self):
    
        # Build SPN
        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))
        root = SumNode(leaf1,leaf2)
        spn = SumProductNetwork(root,input_shape=(1,2))

        # Manually set sum node's weights
        spn.set_sum_weights(root,[[-0.1, 0.7]])
        #spn.set_sum_weights(root,[[-0.1, 0.701]])  #OK
        #spn.set_sum_weights(root,[[-0.1, 0.71]])   #failed

        # Perform inference
        dataset = [[[0.1, 0.2]], [[1.0, 0.4]], [[0.5, 0.9]]]
        root_value = spn.inference(dataset)

        # Assert
        self.assertTrue(np.allclose(root_value,[[-1.61015141],[-0.91415322],[-0.1059947]]))


    def test_compute_value_2D(self):

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))
        leaf3 = InputLeafNode((1,0))
        leaf4 = InputLeafNode((1,1))

        root = SumNode(leaf1,leaf2, leaf3, leaf4)
        spn = SumProductNetwork(root,input_shape=(2,2))
        
        spn.set_sum_weights(root,[[-0.1, 1.0, 0.8, -0.3]])
        
        dataset = [[[0.1, 0.2],[1.0, 0.9]], [[1.0, 0.4],[0.2, 0.9]], [[0.5, 0.9],[0.5, 0.6]]]

        root_value = spn.inference(dataset)

        self.assertTrue(np.allclose(root_value,[[-0.58789784], [-1.16532862], [-0.32568729]]))


    def test_compute_mpe_mask_1D(self):

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))

        root = SumNode(leaf1,leaf2)
        spn = SumProductNetwork(root,input_shape=(1,2))
        
        spn.set_sum_weights(root,[[0.5, 0.5]])
        
        dataset = np.array([[[0.8, 0.2]], [[0.3, 0.7]], [[0.35, 0.34]]])

        root_value = spn.inference(dataset)

        mpe_mask_leaves = spn.mpe_inference(dataset)
        
        expected_output = np.array([[[0.8, 0.]], [[0., 0.7]], [[0.35, 0.]]])
        
        self.assertTrue(np.allclose(mpe_mask_leaves,expected_output))


    def test_compute_mpe_mask_2D(self):

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))
        leaf3 = InputLeafNode((1,0))
        leaf4 = InputLeafNode((1,1))
        
        root = SumNode(leaf1, leaf2, leaf3, leaf4)
        spn = SumProductNetwork(root,input_shape=(2,2))
        
        spn.set_sum_weights(root,[[0.01, 0.1, 0.2, 0.3]])
        
        dataset = np.array([[[1.0, 0.8],[0.6, 0.3]], [[1.0, 0.4],[0.2, 0.9]], [[0.5, 0.9],[0.5, 0.6]]])
        
        root_value = spn.inference(dataset)

        mpe_mask_leaves = spn.mpe_inference(dataset)
        
        expected_output = np.array([[[0., 0.],[0.6, 0.]], [[0., 0.],[0., 0.9]], [[0., 0.],[0., 0.6]]])
        
        self.assertTrue(np.allclose( mpe_mask_leaves,expected_output ))




class ProductNodeTest(unittest.TestCase):


    def test_compute_value_1D(self):

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))

        root = ProductNode(leaf1,leaf2)
        spn = SumProductNetwork(root,input_shape=(1,2))
        
        dataset = [[[0.1, 0.2]], [[1.0, 0.4]], [[0.5, 0.9]]]#input shape must be [N,m,n]

        root_value = spn.inference(dataset)

        self.assertTrue(np.allclose(root_value,[[-3.91202307], [-0.9162907 ], [-0.79850775]]))


    def test_compute_value_2D(self):

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))
        leaf3 = InputLeafNode((1,0))
        leaf4 = InputLeafNode((1,1))

        root = ProductNode(leaf1,leaf2, leaf3, leaf4)
        spn = SumProductNetwork(root,input_shape=(2,2))
        
        dataset = [[[0.1, 0.2],[1.0, 0.9]], [[1.0, 0.4],[0.2, 0.9]], [[0.5, 0.9],[0.5, 0.6]]]

        root_value = spn.inference(dataset)

        self.assertTrue(np.allclose(root_value,[[-4.0173836], [-2.6310892], [-2.0024805]]))


    def test_compute_mpe_mask_1D(self):

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))

        root = ProductNode(leaf1,leaf2)
        spn = SumProductNetwork(root, input_shape=(1,2))
        dataset = np.array([[[0.8, 0.2]], [[0.3, 0.7]], [[0.35, 0.34]]])
        
        root_value = spn.inference(dataset)

        mpe_mask_leaves = spn.mpe_inference(dataset)

        expected_output = np.array([[[0.8, 0.2]], [[0.3, 0.7]], [[0.35, 0.34]]])

        self.assertTrue(np.allclose(mpe_mask_leaves,expected_output))


    def test_compute_mpe_mask_2D(self):

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))
        leaf3 = InputLeafNode((1,0))
        leaf4 = InputLeafNode((1,1))

        root = ProductNode(leaf1,leaf2, leaf3, leaf4)
        spn = SumProductNetwork(root,input_shape=(2,2))
        
        dataset = np.array([[[0.1, 0.2],[1.0, 0.9]], [[1.0, 0.4],[0.2, 0.9]], [[0.5, 0.9],[0.5, 0.6]]])

        root_value = spn.inference(dataset)

        mpe_mask_leaves = spn.mpe_inference(dataset)
        
        expected_output = np.array([[[0.1, 0.2],[1.0, 0.9]], [[1.0, 0.4],[0.2, 0.9]], [[0.5, 0.9],[0.5, 0.6]]])
        
        #self.assertTrue(np.allclose( mpe_mask_leaves, [ [[1., 1.], [1., 1.]], [[1., 1.], [1., 1.]], [[1., 1.], [1., 1.]] ] ))
        self.assertTrue(np.allclose(mpe_mask_leaves, expected_output))


class SumProductTest(unittest.TestCase):

    def test_compute_value_1D(self):

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,0))
        leaf3 = InputLeafNode((0,1))
        leaf4 = InputLeafNode((0,1))


        prod1 = ProductNode(leaf1,leaf3)
        prod2 = ProductNode(leaf2,leaf4)
        sum1 = SumNode(prod1,prod2)

        spn = SumProductNetwork(sum1, input_shape=(1,2))

        spn.set_sum_weights(sum1,[[0.1, 0.5]])
        
        dataset = [[[0.1, 0.2]], [[0.9, 0.6]]]
        
        root_value = spn.inference(dataset)

        self.assertTrue(np.allclose(root_value,[[-3.91202307],[-0.61618608]]))
        #self.assertTrue(np.allclose(root_value,[[-3.912],[-0.61618608]]))  #this was OK
        
    def test_compute_value_1D_rectangle_1_3_1(self):
        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))
        leaf3 = InputLeafNode((0,2))
        leaf4 = InputLeafNode((0,0))
        leaf5 = InputLeafNode((0,1))
        leaf6 = InputLeafNode((0,2))
        prod1 = ProductNode(leaf2,leaf3)
        prod2 = ProductNode(leaf4,leaf5)
        sum1 = SumNode(prod1)
        sum2 = SumNode(prod2)
        prod3 = ProductNode(leaf1,sum1)
        prod4 = ProductNode(sum2,leaf6)
        root = SumNode(prod3,prod4)

        spn = SumProductNetwork(root, input_shape=(1,3))
        spn.set_sum_weights(sum1,[[0.4]])
        spn.set_sum_weights(sum2,[[0.7]])
        spn.set_sum_weights(root,[[0.6, 0.4]])

        dataset = [[[0.3, 0.5, 1.0]], [[0.05, 0.2, 0.4]]]
        
        root_value = spn.inference(dataset)
        #print('>>>>>>>', root_value)
        self.assertTrue(np.allclose(root_value,[[-1.8971199],[-5.5214609]]))


    def test_compute_value_2D(self):

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,0))
        leaf3 = InputLeafNode((0,1))
        leaf4 = InputLeafNode((0,1))
        leaf5 = InputLeafNode((1,0))
        leaf6 = InputLeafNode((1,0))
        leaf7 = InputLeafNode((1,1))
        leaf8 = InputLeafNode((1,1))
        sum1 = SumNode(leaf1,leaf2)
        sum2 = SumNode(leaf3,leaf4)
        sum3 = SumNode(leaf5,leaf6)
        sum4 = SumNode(leaf7,leaf8)
        root = ProductNode(sum1,sum2,sum3,sum4)
        
        spn = SumProductNetwork(root, input_shape=(2,2))

        spn.set_sum_weights(sum1,[[-0.5,0.1]])
        spn.set_sum_weights(sum2,[[0.1,-0.3]])
        spn.set_sum_weights(sum3,[[0.1,0.9]])
        spn.set_sum_weights(sum4,[[0.2,1.0]])
        
        dataset = [ [[0.3, 0.5],[1.0, 0.1]] , [[0.05, 0.2],[0.4, 0.7]] ]
        root_value = spn.inference(dataset)
        #print('>>>>>>>>>>', root_value)
        self.assertTrue(np.allclose(root_value,[[-4.199705], [-5.878136]]))


    #TODO TOFINISH
    # def test_compute_mpe_mask_1D(self):

    #   leaf1 = InputLeafNode(0)
    #   leaf2 = InputLeafNode(1)
    #   leaf3 = InputLeafNode(0)
    #   leaf4 = InputLeafNode(1)


    #   prod1 = ProductNode(leaf1,leaf3)
    #   prod2 = ProductNode(leaf2,leaf4)
    #   sum1 = SumNode(prod1,prod2)

    #   spn = SumProductNetwork(sum1, input_shape=(None,2))

    #   spn.set_sum_weights(sum1,[[0.1, 0.5]])

    #   dataset = np.array([[0.1, 0.2], [0.9, 0.6]])

    #   learner = PercentileGaussianLeafLearning(spn=spn,data=dataset,n_components=2)
    #   learner.fit()#funcao nao está funcionando
        
    #   root_value = spn.inference(dataset)
    #   mpe_mask = spn.mpe_inference(dataset)

    #   print("***** RESULT MPE MASK")
    #   print(mpe_mask)


    #TODO TOFINISH
    # def test_compute_mpe_mask_2D(self):

    #   leaf1 = InputLeafNode((0,0))
    #   leaf2 = InputLeafNode((0,0))
    #   leaf3 = InputLeafNode((0,1))
    #   leaf4 = InputLeafNode((0,1))
    #   leaf5 = InputLeafNode((1,0))
    #   leaf6 = InputLeafNode((1,0))
    #   leaf7 = InputLeafNode((1,1))
    #   leaf8 = InputLeafNode((1,1))
    #   sum1 = SumNode(leaf1,leaf2)
    #   sum2 = SumNode(leaf3,leaf4)
    #   sum3 = SumNode(leaf5,leaf6)
    #   sum4 = SumNode(leaf7,leaf8)
    #   prod_1 = ProductNode(sum1,sum2,sum3,sum4)

    #   leaf9 = InputLeafNode((0,0))
    #   leaf10 = InputLeafNode((0,0))
    #   leaf11 = InputLeafNode((0,1))
    #   leaf12 = InputLeafNode((0,1))
    #   leaf13 = InputLeafNode((1,0))
    #   leaf14 = InputLeafNode((1,0))
    #   leaf15 = InputLeafNode((1,1))
    #   leaf16 = InputLeafNode((1,1))
    #   sum5 = SumNode(leaf9,leaf10)
    #   sum6 = SumNode(leaf11,leaf12)
    #   sum7 = SumNode(leaf13,leaf14)
    #   sum8 = SumNode(leaf15,leaf16)
    #   prod_2 = ProductNode(sum5,sum6,sum7,sum8)

    #   root = SumNode(prod_1, prod_2)
        
    #   spn = SumProductNetwork(root, input_shape=(None,2,2))

    #   spn.set_sum_weights(sum1,[[-0.5,0.1]])
    #   spn.set_sum_weights(sum2,[[0.1,-0.3]])
    #   spn.set_sum_weights(sum3,[[0.1,0.9]])
    #   spn.set_sum_weights(sum4,[[0.2,1.0]])
    #   spn.set_sum_weights(sum5,[[-0.5,0.1]])
    #   spn.set_sum_weights(sum6,[[0.1,-0.3]])
    #   spn.set_sum_weights(sum7,[[0.1,0.9]])
    #   spn.set_sum_weights(sum8,[[0.2,1.0]])
    #   spn.set_sum_weights(root,[[0.2,1.0]])
        
    #   dataset = [ [[0.3, 0.5],[1.0, 0.1]] , [[0.05, 0.2],[0.4, 0.7]] ]
    #   root_value = spn.inference(dataset)
    #   mpe_mask = spn.mpe_inference(dataset)

    #   print("***** RESULT MPE MASK")
    #   print(mpe_mask)





class StructuralLearningTest(unittest.TestCase):
    #tests for get_spn using graphs(verifying if it's valid too with the function is_valid)
        # case name starts with test_get_spn_ and the end means:
                    #input_shape[0]_input_shape[1]_leaf_channels_sum_node_channels_resolution_size[0]_resolution_size[1]_type_cuts_selection
    #default and optional values:
            #sum_node_channels=1, leaf_channel=1, resolution_size=(1,1), _type_cuts_selection='all'
    def test_get_spn_graph1(self):
        G = nx.DiGraph()
        G.add_node(0,type='sum',label='+')
        G.add_node(1,type='product',label='*')
        G.add_node(2,type='leaf',scope=(0,0),label='(0,0)')
        G.add_edges_from([(0,1),(1,2)])
        
        learner = StructuralLearning(input_shape=(1,1))
        learner.graph = G#don't call function get_spn without setting a graph
        
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [1, 1, 1]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,1, 1, 1])
        self.assertTrue(spn.is_valid())

    def test_get_spn_graph2(self):
        G = nx.DiGraph()
        G.add_nodes_from([5,6,7,8,9,10,13],type='sum',label='+')
        G.add_nodes_from([11,12],type='product',label='*')
        G.add_nodes_from([0,1,2,3,4],type='leaf',scope=(0,0),label="(0,0)")
        G.add_edges_from([(13,11),(13,12),(11,10),(11,9),(11,8),(12,7),(12,6),(12,5),(5,0),(6,1),(7,2),(8,3),(9,4),(10,0)])
        #nx.drawing.nx_pydot.write_dot(G, "materials/test_get_spn_graph2.dot")
        
        learner = StructuralLearning(input_shape=(1,5))
        learner.graph = G#don't call function get_spn without setting a graph
        
        spn = learner.get_spn()
        #SumProductNetwork.save(spn, "materials")
        #[#leaves, #sum_nodes, #product_nodes] = [5, 7, 2]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,5, 7, 2])
        self.assertFalse(spn.is_valid())
        
    #tests for get_spn using Poon and Domingos
    def test_get_spn_PoonAndDomingos_1_2(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(1,2),leaf_channels=1,sum_node_channels=1)
        spn = learner.get_spn()#functions calls learn_network
        #[#leaves, #sum_nodes, #product_nodes] = [2, 1, 1]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,2, 1, 1])
        self.assertTrue(spn.is_valid())
    
    def test_get_spn_PoonAndDomingos_1_3(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(1,3),leaf_channels=1,sum_node_channels=1)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [3, 3, 4] 
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,3, 3, 4])
        self.assertTrue(spn.is_valid())

    def test_get_spn_PoonAndDomingos_1_4(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(1,4),leaf_channels=1,sum_node_channels=1)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [4, 6, 10]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,4, 6, 10])
        self.assertTrue(spn.is_valid())

    def test_get_spn_PoonAndDomingos_2_2(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,2),leaf_channels=1,sum_node_channels=1)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [4, 5, 6]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,4, 5, 6])
        self.assertTrue(spn.is_valid())

    def test_get_spn_PoonAndDomingos_2_2_2_2(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,2),leaf_channels=2, sum_node_channels=2)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [8, 9, 24]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,8, 9, 24])
        self.assertTrue(spn.is_valid())
    
    def test_get_spn_PoonAndDomingos_2_3(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,3),leaf_channels=1,sum_node_channels=1)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [6,12,18]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,6,12,18])
        self.assertTrue(spn.is_valid())

    def test_get_spn_PoonAndDomingos_2_4(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,4),leaf_channels=1,sum_node_channels=1)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [8, 22, 40]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,8, 22, 40])
        self.assertTrue(spn.is_valid())
    
    def test_get_spn_PoonAndDomingos_3_3(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(3,3),leaf_channels=1,sum_node_channels=1)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [9, 27, 48]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,9, 27, 48])
        self.assertTrue(spn.is_valid())

    def test_get_spn_PoonAndDomingos_4_4(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1,sum_node_channels=1)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16, 84, 200]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16, 84, 200])
        self.assertTrue(spn.is_valid())

    def test_get_spn_PoonAndDomingos_8_8(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=1,sum_node_channels=1)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [64, 1232, 6048]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,64, 1232, 6048])
        self.assertTrue(spn.is_valid())

        
    def test_get_spn_PoonAndDomingos_12_12(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(12,12),leaf_channels=1,sum_node_channels=1)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [144,5940,44616]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,144,5940,44616])
        self.assertTrue(spn.is_valid())      
    # ------------------------     TAkes time----------------------------
    # def test_get_spn_PoonAndDomingos_24_24(self):
    #     learner = PoonAndDomingosStructuralLearning(input_shape=(24,24),leaf_channels=1,sum_node_channels=1)
    #     spn = learner.get_spn()
    #     #[#leaves, #sum_nodes, #product_nodes] = [576,89424,1380000]
    #     self.assertEqual([len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[576,89424,1380000])# 90s
    # ------------------------     TAkes time

    #cases where we force to do a fine composition over all rectangle -> must be equal of resolution 1x1
    def test_get_spn_PoonAndDomingos_2_3_1_1_2_3(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,3),leaf_channels=1,sum_node_channels=1,resolution_size=(2,3))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [6,12,18] 
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,6,12,18])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_2_4_1_1_2_4(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,4),leaf_channels=1,sum_node_channels=1,resolution_size=(2,4))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [8, 22, 40] 
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,8,22,40])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_3_3_1_1_3_3(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(3,3),leaf_channels=1,sum_node_channels=1,resolution_size=(3,3))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [9,27,48] 
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,9,27,48])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_1_1_4_4(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1,sum_node_channels=1,resolution_size=(4,4))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16, 84, 200] obs: must be the same of 4x4(1x1)
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16, 84, 200])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_8_8_1_1_8_8(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=1,sum_node_channels=1,resolution_size=(8,8))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [64, 1232, 6048]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,64, 1232, 6048])
        self.assertTrue(spn.is_valid())
    
    #------------------------------------- using resolution
    def test_get_spn_PoonAndDomingos_2_4_1_1_2_2(self):
        learner1 = PoonAndDomingosStructuralLearning(input_shape=(2,4),leaf_channels=1,sum_node_channels=1,resolution_size=(2,2))
        spn = learner1.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [8, 11, 13]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,8, 11, 13])
        self.assertTrue(spn.is_valid())
    #still crashing because resolutions that doesn't fit the image are not done
    # def test_get_spn_PoonAndDomingos_3_3_1_1_2_2(self):
    #   learner = PoonAndDomingosStructuralLearning(input_shape=(3,3),leaf_channels=1,sum_node_channels=1,resolution_size=(2,2))
    #   spn = learner.get_spn()
    #   #[#leaves, #sum_nodes, #product_nodes] = [104, 65, 84]
    #   #self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,104, 65, 84])     

    def test_get_spn_PoonAndDomingos_4_4_1_1_2_2(self):
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1,sum_node_channels=1,resolution_size=(2,2))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16, 25, 30]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16, 25, 30])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_8_8_1_1_2_2(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=1,sum_node_channels=1,resolution_size=(2,2))
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [64,164,296]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,64,164,296])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_8_8_1_1_4_4(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=1,sum_node_channels=1,resolution_size=(4,4))
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [64,341,806]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,64,341,806])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_16_16_1_1_4_4(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(16,16),leaf_channels=1,sum_node_channels=1,resolution_size=(4,4))
        spn = learner3.get_spn() # 1.32471646s
        #[#leaves, #sum_nodes, #product_nodes] = [256,1428,3400]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,256,1428,3400]) #1.39390239s
        self.assertTrue(spn.is_valid()) # 0.48852492s
        
    def test_get_spn_PoonAndDomingos_32_32_1_1_4_4(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(32,32),leaf_channels=1,sum_node_channels=1,resolution_size=(4,4))
        spn = learner3.get_spn() # before: 16s(c.instance: 2.6s) now: total: 3.56s
        #[#leaves, #sum_nodes, #product_nodes] = [1024,6608,18848]
        self.assertEqual([len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1024,6608,18848])# before: 38.3s    now: 0.20452413300517946
        self.assertTrue(spn.is_valid())# 13.7s
        
    #------------------ changing the leaf_channels and sum_node_channels
    def test_get_spn_PoonAndDomingos_2_2_1_2_2_2(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,2),leaf_channels=1,sum_node_channels=2,resolution_size=(2,2))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [4,9,45]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,4,9,12])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_2_2_1_10_2_2(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,2),leaf_channels=1,sum_node_channels=10,resolution_size=(2,2))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [4,9,45]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,4,41,204])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_2_2_2_1_2_2(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,2),leaf_channels=2,sum_node_channels=1,resolution_size=(2,2))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [8,5,18]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,8,5,18])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_2_2_10_1_2_2(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,2),leaf_channels=10,sum_node_channels=1,resolution_size=(2,2))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [8,5,402]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,40,5,402])
        self.assertTrue(spn.is_valid())
    
    def test_get_spn_PoonAndDomingos_2_3_1_2_2_3(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,3),leaf_channels=1,sum_node_channels=2,resolution_size=(2,3))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [6,23,43]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,6,23,43])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_2_4_1_2_2_4(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,4),leaf_channels=1,sum_node_channels=2,resolution_size=(2,4))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [8,43,106]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,8,43,106])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_1_2_4_4(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1,sum_node_channels=2,resolution_size=(4,4))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16,167,632]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16,167,632])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_1_3_4_4(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1,sum_node_channels=3,resolution_size=(4,4))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16,250,1320]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16,250,1320])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_1_10_4_4(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1,sum_node_channels=10,resolution_size=(4,4))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16,831,13304]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16,831,13304])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_2_1_4_4(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=2,sum_node_channels=1,resolution_size=(4,4))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [32,84,320]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,32,84,320])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_3_1_4_4(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=3,sum_node_channels=1,resolution_size=(4,4))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [48,84,488]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,48,84,488])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_10_1_4_4(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=10,sum_node_channels=1,resolution_size=(4,4))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [160,84,3008]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,160,84,3008])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_2_4_1_2_2_2(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,4),leaf_channels=1,sum_node_channels=2,resolution_size=(2,2))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [8,21,28]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,8,21,28])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_2_4_1_10_2_2(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,4),leaf_channels=1,sum_node_channels=10,resolution_size=(2,2))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [8,101,508]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,8,101,508])
        self.assertTrue(spn.is_valid())

    def test_get_spn_PoonAndDomingos_2_4_10_1_2_2(self): 
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,4),leaf_channels=10,sum_node_channels=1,resolution_size=(2,2))
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [80,11,805]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,80,11,805])
        self.assertTrue(spn.is_valid())

    def test_get_spn_PoonAndDomingos_4_4_1_2_2_2(self):
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1, sum_node_channels=2,resolution_size=(2,2))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16,49,72]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16,49,72])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_1_10_2_2(self):
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1, sum_node_channels=10,resolution_size=(2,2))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16,241,1416]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16,241,1416])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_2_1_2_2(self):
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=2, sum_node_channels=1,resolution_size=(2,2))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [32,25,78]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,32,25,78])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_10_1_2_2(self):
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=10, sum_node_channels=1,resolution_size=(2,2))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [160,25,1614]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,160,25,1614])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_1_2_1_1(self): 
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1, sum_node_channels=2,resolution_size=(1,1))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16,167,632]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16,167,632])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_1_3_1_1(self): 
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1, sum_node_channels=3,resolution_size=(1,1))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16, 250, 1320] 
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16, 250, 1320])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_1_10_1_1(self): 
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1, sum_node_channels=10,resolution_size=(1,1))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16, 831, 13304] 
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16, 831, 13304])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_2_1_1_1(self): 
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=2, sum_node_channels=1,resolution_size=(1,1))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [32, 84, 320] 
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,32, 84, 320])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_3_1_1_1(self): 
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=3, sum_node_channels=1,resolution_size=(1,1))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [48, 84, 488] 
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,48, 84, 488])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_10_1_1_1(self): 
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=10, sum_node_channels=1,resolution_size=(1,1))
        spn = learner2.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [160, 84, 3008] 
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,160, 84, 3008])
        self.assertTrue(spn.is_valid())

    def test_get_spn_PoonAndDomingos_8_8_2_1_2_2(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=2, sum_node_channels=1,resolution_size=(2,2))
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [128,164,488]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,128,164,488])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_8_8_2_2_2_2(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=2, sum_node_channels=2,resolution_size=(2,2))
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [128,327,1184]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,128,327,1184])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_8_8_10_1_2_2(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=10, sum_node_channels=1,resolution_size=(2,2))
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [640,164,6632]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,640,164,6632])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_16_16_1_2_4_4(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(16,16),leaf_channels=1,sum_node_channels=2,resolution_size=(4,4))
        spn = learner3.get_spn() # s
        #[#leaves, #sum_nodes, #product_nodes] = [256,2855,10912]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,256,2855,10912]) 
        self.assertTrue(spn.is_valid()) 
    def test_get_spn_PoonAndDomingos_16_16_1_3_4_4(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(16,16),leaf_channels=1,sum_node_channels=3,resolution_size=(4,4))
        spn = learner3.get_spn() # s
        #[#leaves, #sum_nodes, #product_nodes] = [256,4282,22920]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,256,4282,22920]) 
        self.assertTrue(spn.is_valid()) 
    # ---------------------   BIG CASES
    # def test_get_spn_PoonAndDomingos_16_16_1_10_4_4(self):
    #     learner3 = PoonAndDomingosStructuralLearning(input_shape=(16,16),leaf_channels=1,sum_node_channels=10,resolution_size=(4,4))
    #     timers = TicTac()
    #     timers.tic()
    #     timers.tic()
    #     spn = learner3.get_spn() # s
    #     print('get_spn: ',timers.toc())
    #     #[#leaves, #sum_nodes, #product_nodes] = [256,14271,232864]
    #     timers.tic()   
    #     self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,256,14271,232864])# s
    #     print('asserting: ',timers.toc())
    #     timers.tic()
    #     self.assertTrue(spn.is_valid()) # s
    #     print('asserting if is valid: ', timers.toc())
    #     print('Total duration: ', timers.toc()) # 
    # def test_get_spn_PoonAndDomingos_64_64_1_1_4_4(self):
    #     learner3 = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=1, sum_node_channels=1,resolution_size=(4,4))
    #     timers = TicTac()
    #     timers.tic()
    #     timers.tic()
    #     spn = learner3.get_spn() # 22.13491906499985s(creating instance: 11.26446256100462)
    #     print('get_spn: ',timers.toc())
    #     #[#leaves, #sum_nodes, #product_nodes] = [4096,39744,236160]
    #     timers.tic()   
    #     self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,4096,39744,236160]) # 1.27s
    #     print('asserting: ',timers.toc())
    #     # timers.tic()
    #     # self.assertTrue(spn.is_valid())# 145s
    #     # print('asserting if is valid: ', timers.toc())
    #     print('Total duration: ', timers.toc()) # 

    # def test_get_spn_PoonAndDomingos_64_64_4_1_4_4(self): 
    #     learner3 = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=4, sum_node_channels=1,resolution_size=(4,4))
    #     timers = TicTac()
    #     timers.tic()
    #     timers.tic()
    #     spn = learner3.get_spn() # 5977s // 58.96s(new method!!)100x better
    #     print('get_spn: ',timers.toc()) # 60s(creating nodes: 15.5, creating instance: 42)
    #     #[#leaves, #sum_nodes, #product_nodes] = [16384,39744,365184]
    #     timers.tic()   
    #     self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16384,39744,365184]) # 1.8s
    #     print('asserting: ',timers.toc())
    #     #timers.tic()
    #     #self.assertTrue(spn.is_valid())# 147s
    #     #print('asserting if is valid: ', timers.toc())
    #     print('Total duration: ', timers.toc()) # 
    # def test_get_spn_PoonAndDomingos_64_64_4_2_4_4(self):
    #     learner3 = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=4,sum_node_channels=2,resolution_size=(4,4))
    #     timers = TicTac()
    #     timers.tic()
    #     timers.tic()
    #     spn = learner3.get_spn() # 84s(creating nodes: 38.72, creating instance: 46.3)  ----- memory: 8Gb
    #     print('get_spn: ',timers.toc())
    #     #[#leaves, #sum_nodes, #product_nodes] = [16384,79487,1067520]
    #     timers.tic()   
    #     self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 16384,79487,1067520]) # 4.9s
    #     print('asserting: ',timers.toc())
    #     # timers.tic()
    #     # self.assertTrue(spn.is_valid())# s
    #     #print('asserting if is valid: ', timers.toc())
    #     print('Total duration: ', timers.toc()) # s
    
    # def test_get_spn_PoonAndDomingos_64_64_4_3_4_4(self):
    #     learner3 = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=4,sum_node_channels=3,resolution_size=(4,4))
    #     timers = TicTac()
    #     timers.tic()
    #     timers.tic()
    #     spn = learner3.get_spn() # 125.8s(creating nodes: 78.4, creating instance: 47.4) ----- memory: 11Gb
    #     print('get_spn: ',timers.toc())
    #     #[#leaves, #sum_nodes, #product_nodes] = [16384,119230,2205312]
    #     # timers.tic()   
    #     self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16384,119230,2205312])
    #     # print('>>>>',[len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))])
    #     # print('asserting: ',timers.toc())
    #     # timers.tic()
    #     # self.assertTrue(spn.is_valid())# s
    #     # print('asserting if is valid: ', timers.toc())
    #     print('Total duration: ', timers.toc()) # 125.88
    # def test_get_spn_PoonAndDomingos_64_64_4_4_4_4(self):
    #     learner3 = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=4,sum_node_channels=4,resolution_size=(4,4))
    #     timers = TicTac()
    #     timers.tic()
    #     timers.tic()
    #     spn = learner3.get_spn() # 255(creating nodes: 134, creating instance: 47.4) ----- memory: 14Gb
    #     print('get_spn: ',timers.toc())
    #     #[#leaves, #sum_nodes, #product_nodes] = [16384,158973,3778560]
    #     timers.tic()   
    #     self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,16384,158973,3778560])
    #     print('asserting: ',timers.toc())
    #     # timers.tic()
    #     # self.assertTrue(spn.is_valid())# s
    #     # print('asserting if is valid: ', timers.toc())
    #     print('Total duration: ', timers.toc()) # 181

    #------------------------------------doing only the central cuts
    def test_get_spn_PoonAndDomingos_2_2_1_1_1_1_central(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(2,2),leaf_channels=1, sum_node_channels=1,resolution_size=(1,1),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [4,5,6]
        self.assertEqual([len(spn.get_nodes_by_type("root")), len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,4,5,6])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_3_3_1_1_1_1_central(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(3,3),leaf_channels=1, sum_node_channels=1,resolution_size=(1,1),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [9, 27, 48]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 9, 27, 48])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_3_3_1_1_1_1_random(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(3,3),leaf_channels=1, sum_node_channels=1,resolution_size=(1,1),type_cuts_selection='random')
        G = learner3.get_structure()
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [9, 8, 8]   case all cuts: [16, 84, 200]   now: [16, 84, 160]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 9, 8, 8])
        self.assertTrue(spn.is_valid())
    def test_get_spn_PoonAndDomingos_4_4_1_1_1_1_central(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1, sum_node_channels=1,resolution_size=(1,1),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [16, 33, 42]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 16, 33, 42])
        self.assertTrue(spn.is_valid())
    #try it! total = 11601
    def test_get_spn_PoonAndDomingos_18_18_4_2_1_1_central(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(18,18),leaf_channels=4, sum_node_channels=2,resolution_size=(1,1),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = []   1296, 1801, 8504    
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 16, 33, 42])
        #self.assertTrue(spn.is_valid())
    #try it! total = 14689
    def test_get_spn_PoonAndDomingos_16_16_2_2_4_4_central(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(16,16),leaf_channels=3, sum_node_channels=3,resolution_size=(4,4),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [1024, 2241, 11424]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 16, 33, 42])
        #self.assertTrue(spn.is_valid())
    #try it! total = 14689
    def test_get_spn_PoonAndDomingos_16_16_4_4_4_4_central(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(16,16),leaf_channels=4, sum_node_channels=4,resolution_size=(4,4),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [1024, 2241, 11424]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 16, 33, 42])
        #self.assertTrue(spn.is_valid())
    #try it! total = 15599
    def test_get_spn_PoonAndDomingos_28_28_2_2_4_4_central(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(28,28),leaf_channels=2, sum_node_channels=2,resolution_size=(4,4),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [1568, 3783, 10248]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 16, 33, 42])
        #self.assertTrue(spn.is_valid())
    #try it! total = 19350
    def test_get_spn_PoonAndDomingos_28_28_4_1_4_4_central(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(28,28),leaf_channels=4, sum_node_channels=1,resolution_size=(4,4),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [3136, 1892, 14322]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 16, 33, 42])
        #self.assertTrue(spn.is_valid())
    #try it! total = 10291
    def test_get_spn_PoonAndDomingos_32_32_2_1_4_4_central(self):     # FULANO
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(32,32),leaf_channels=2, sum_node_channels=1,resolution_size=(4,4),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [2048, 2273, 5970]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 16, 33, 42])
        #self.assertTrue(spn.is_valid())
    #try it   total = 18185
    def test_get_spn_PoonAndDomingos_32_32_2_2_4_4_central(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(32,32),leaf_channels=2, sum_node_channels=2,resolution_size=(4,4),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [2048, 4545, 11592]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 16, 33, 42])
        #self.assertTrue(spn.is_valid())
    #try it   total = 24931
    def test_get_spn_PoonAndDomingos_64_64_1_1_4_4_central(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=1, sum_node_channels=1,resolution_size=(4,4),type_cuts_selection='central')
        spn = learner3.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [4096, 9153, 11682]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1, 16, 33, 42])
        #self.assertTrue(spn.is_valid())

    #------------------------------------------------------tests for get_spn using Conv2D
    def test_get_spn_Conv2D_1_4_2(self):
        learner = Conv2DStructuralLearning(input_shape=(1,4),leaf_channels=2)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [8, 13, 6]
        self.assertEqual([len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[8, 13, 6])
        self.assertTrue(spn.is_valid())
    
    def test_get_spn_Conv2D_1_5_2(self):
        learner = Conv2DStructuralLearning(input_shape=(1,5),leaf_channels=2)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [10, 21, 12]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,10, 21, 12])
        self.assertTrue(spn.is_valid())

    def test_get_spn_Conv2D_3_3_2(self):
        learner = Conv2DStructuralLearning(input_shape=(3,3),leaf_channels=2)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [18, 27, 10]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,18, 27, 10])
        self.assertTrue(spn.is_valid())


    def test_get_spn_Conv2D_4_4_2(self):
        learner = Conv2DStructuralLearning(input_shape=(4,4),leaf_channels=2)
        spn = learner.get_spn()
        #[#leaves, #sum_nodes, #product_nodes] = [32, 41, 10]
        self.assertEqual([len(spn.get_nodes_by_type("root")),len(spn.get_nodes_by_type("leaf")), len(spn.get_nodes_by_type("sum")), len(spn.get_nodes_by_type("product"))],[1,32, 41, 10])
        self.assertTrue(spn.is_valid())



class Conv2DStructuralLearningTest(unittest.TestCase):
    #tests to verify if the right graph is being generated - Conv2D
    def test_Conv2D_learn_network_1_4_2(self):
        learner = Conv2DStructuralLearning(input_shape=(1,4),leaf_channels=2)
        G = learner.get_structure()#this function calls learn_network
        #[#leaves, #sum_nodes, #product_nodes] = [8, 13, 6]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[8, 13, 6])
    
    def test_Conv2D_learn_network_1_5_2(self):
        learner = Conv2DStructuralLearning(input_shape=(1,5),leaf_channels=2)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [10, 21, 12]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[10, 21, 12])

    def test_Conv2D_learn_network_3_3_2(self):
        learner = Conv2DStructuralLearning(input_shape=(3,3),leaf_channels=2)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [18, 27, 10]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[18, 27, 10])


    def test_Conv2D_learn_network_4_4_2(self):
        learner = Conv2DStructuralLearning(input_shape=(4,4),leaf_channels=2)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [32, 41, 10]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[32, 41, 10])

class PoonAndDomingosStructuralLearningTest(unittest.TestCase):
    #tests to verify if the right graph is being generated - Rectangles
        # case name starts with test_PoonAndDom_learn_network and the end means:
                    #input_shape[0]_input_shape[1]_leaf_channels_sum_node_channels_resolution_size[0]_resolution_size[1]_type_cuts_selection
    #default and optional values:
            #sum_node_channels=1, leaf_channel=1, resolution_size=(1,1), _type_cuts_selection='all'
    def test_PoonAndDom_learn_network_1_2(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(1,2),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [2, 1, 1]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[2, 1, 1])
    
    def test_PoonAndDom_learn_network_1_3(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(1,3),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [3, 3, 4] 
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[3, 3, 4])

    def test_PoonAndDom_learn_network_1_4(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(1,4),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [4, 6, 10]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[4, 6, 10])

    def test_PoonAndDom_learn_network_2_2(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,2),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [4, 5, 6]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[4, 5, 6])

    def test_PoonAndDom_learn_network_2_2_2_2(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,2),leaf_channels=2,sum_node_channels=2)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [8, 9, 24]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[8, 9, 24])
    
    def test_PoonAndDom_learn_network_2_3(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,3),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [6,12,18]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[6,12,18])
    def test_PoonAndDom_learn_network_2_4(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,4),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [8, 22, 40]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[8, 22, 40])
    def test_PoonAndDom_learn_network_3_3(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(3,3),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [9,27,48]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[9,27,48])
    def test_PoonAndDom_learn_network_4_4(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [16, 84, 200]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[16, 84, 200])
    def test_PoonAndDom_learn_network_8_8(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [64, 1232, 6048]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[64, 1232, 6048])
    def test_PoonAndDom_learn_network_12_12(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(12,12),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [144,5940,44616]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[144,5940,44616])
    def test_PoonAndDom_learn_network_24_24(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(24,24),leaf_channels=1,sum_node_channels=1)
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [576,89424,1380000]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[576,89424,1380000])
    #----------------------------   High memory!!!!!!!
    # def test_PoonAndDom_learn_network_28_28(self):
    #     learner = PoonAndDomingosStructuralLearning(input_shape=(28,28),leaf_channels=1,sum_node_channels=1)
    #     G = learner.get_structure()
    #     #[#leaves, #sum_nodes, #product_nodes] = [784,164052,2967048]
    #     self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[784,164052,2967048])# 85s total
    # def test_PoonAndDom_learn_network_32_32(self):
    #     learner = PoonAndDomingosStructuralLearning(input_shape=(32,32),leaf_channels=1,sum_node_channels=1)
    #     G = learner.get_structure()
    #     #[#leaves, #sum_nodes, #product_nodes] = [1024,277760,5761536]
    #     self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[1024,277760,5761536]) # 175s total
    # def test_PoonAndDom_learn_network_64_64(self): # not verified
    #   learner = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=1,sum_node_channels=1)
    #   G = learner.get_structure()
    #   #[#leaves, #sum_nodes, #product_nodes] = [4096,4322304,181708800]
    #   self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[4096,4322304,181708800])
    #----------------------------   High memory!!!!!!!

                         #using resolutions to reduce the number of cuts
    #cases where we force to do a fine composition over all rectangle -> must be equal of resolution 1x1
    def test_PoonAndDom_learn_network_2_3_1_1_2_3(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,3),leaf_channels=1,sum_node_channels=1,resolution_size=(2,3))
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [6,12,18] 
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[6,12,18])       
    def test_PoonAndDom_learn_network_2_4_1_1_2_4(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,4),leaf_channels=1,sum_node_channels=1,resolution_size=(2,4))
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [8, 22, 40] 
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[8, 22, 40])     
    def test_PoonAndDom_learn_network_3_3_1_1_3_3(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(3,3),leaf_channels=1,sum_node_channels=1,resolution_size=(3,3))
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [9,27,48] 
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[9,27,48])       
    def test_PoonAndDom_learn_network_4_4_1_1_4_4(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1,sum_node_channels=1,resolution_size=(4,4))
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [16, 84, 200] obs: must be the same of 4x4(1x1)
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[16, 84, 200])
    def test_PoonAndDom_learn_network_8_8_1_1_8_8(self):
        learner = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=1,sum_node_channels=1,resolution_size=(8,8))
        G = learner.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [64, 1232, 6048]
        self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[64, 1232, 6048])
    
    #-----------------------------------------------------------------------    
    def test_PoonAndDom_learn_network_2_4_1_1_2_2(self):
        learner1 = PoonAndDomingosStructuralLearning(input_shape=(2,4),leaf_channels=1,sum_node_channels=1,resolution_size=(2,2))
        G1 = learner1.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [8, 11, 13]
        self.assertEqual([len([n for n in G1.nodes() if G1.out_degree(n) == 0]), len([n for n in G1.nodes() if G1.nodes[n]['type'] == 'sum']), len([n for n in G1.nodes() if G1.nodes[n]['type'] == 'product'])],[8, 11, 13])       
    # def test_PoonAndDom_learn_network_3_3_1_2_2(self):
    #   learner = PoonAndDomingosStructuralLearning(input_shape=(3,3),leaf_channels=1,sum_node_channels=1,resolution_size=(2,2))
    #   G = learner.get_structure()# 0.00367258s
    #   nx.drawing.nx_pydot.write_dot(G, "g.dot")
    #   #spn = learner.get_spn()# 0.94269496s
    #   #nx.drawing.nx_pydot.write_dot(G, "G1.dot")
    #   #[#leaves, #sum_nodes, #product_nodes] = [104, 65, 84]
    #   self.assertEqual([len([n for n in G.nodes() if G.out_degree(n) == 0]), len([n for n in G.nodes() if G.nodes[n]['type'] == 'sum']), len([n for n in G.nodes() if G.nodes[n]['type'] == 'product'])],[104, 65, 84])       
    def test_PoonAndDom_learn_network_4_4_1_1_2_2(self):
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=1,sum_node_channels=1,resolution_size=(2,2))
        G2 = learner2.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [16, 25, 30]
        self.assertEqual([len([n for n in G2.nodes() if G2.out_degree(n) == 0]), len([n for n in G2.nodes() if G2.nodes[n]['type'] == 'sum']), len([n for n in G2.nodes() if G2.nodes[n]['type'] == 'product'])],[16, 25, 30])
    def test_PoonAndDom_learn_network_8_8_1_1_4_4(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=1,sum_node_channels=1,resolution_size=(4,4))
        G3 = learner3.get_structure()
        self.assertEqual([len([n for n in G3.nodes() if G3.out_degree(n) == 0]), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'sum']), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'product'])],[64,341,806])
    def test_PoonAndDom_learn_network_64_64_1_1_4_4(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=1,sum_node_channels=1,resolution_size=(4,4))
        G3 = learner3.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [4096,39744,236160]
        self.assertEqual([len([n for n in G3.nodes() if G3.out_degree(n) == 0]), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'sum']), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'product'])],[4096,39744,236160])
    def test_PoonAndDom_learn_network_8_8_1_2_4_4(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(8,8),leaf_channels=1,sum_node_channels=2,resolution_size=(4,4))
        G3 = learner3.get_structure()
        self.assertEqual([len([n for n in G3.nodes() if G3.out_degree(n) == 0]), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'sum']), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'product'])],[64,681,2552])
    def test_PoonAndDom_learn_network_4_4_4_20_2_2(self):
        learner2 = PoonAndDomingosStructuralLearning(input_shape=(4,4),leaf_channels=4,sum_node_channels=20,resolution_size=(2,2))
        G2 = learner2.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [64,481,5856]
        self.assertEqual([len([n for n in G2.nodes() if G2.out_degree(n) == 0]), len([n for n in G2.nodes() if G2.nodes[n]['type'] == 'sum']), len([n for n in G2.nodes() if G2.nodes[n]['type'] == 'product'])],[64,481,5856])
    def test_PoonAndDom_learn_network_64_64_2_2_4_4(self):
        learner3 = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=2, sum_node_channels=2,resolution_size=(4,4))
        G3 = learner3.get_structure()
        #[#leaves, #sum_nodes, #product_nodes] = [8192,79487,944640]
        self.assertEqual([len([n for n in G3.nodes() if G3.out_degree(n) == 0]), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'sum']), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'product'])],[8192,79487,944640])# 21s total
    # def test_PoonAndDom_learn_network_64_64_1_3_4_4(self):
    #     learner3 = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=1, sum_node_channels=3,resolution_size=(4,4))
    #     G3 = learner3.get_structure()
    #     #[#leaves, #sum_nodes, #product_nodes] = [4096,119230,2002560]
    #     self.assertEqual([len([n for n in G3.nodes() if G3.out_degree(n) == 0]), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'sum']), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'product'])],[4096,119230,2002560])# 40s total
    # def test_PoonAndDom_learn_network_64_64_4_20_4_4(self): not verified
    #   learner3 = PoonAndDomingosStructuralLearning(input_shape=(64,64),leaf_channels=4, sum_node_channels=20,resolution_size=(4,4))
        
    #   timers = TicTac()
    #   timers.now(0)
    #   G3 = learner3.get_structure()
    #   timers.now(0,'get_structure')
    #   #timers.now(1)
    #   #spn = learner3.get_spn() #15.18008756s create instance
    #   #timers.now(1,'get_spn')
    #   #[#leaves, #sum_nodes, #product_nodes] = [16384,794861,88172544]
    #   timers.now(2)
    #   self.assertEqual([len([n for n in G3.nodes() if G3.out_degree(n) == 0]), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'sum']), len([n for n in G3.nodes() if G3.nodes[n]['type'] == 'product'])],[16384,794861,88172544])
    #   timers.now(2,'asserting')

class SumProductNetworkTest(unittest.TestCase):

	#TODO - Not Working
	# def test_fit_gauss_leave(self):

	# 	leafA_1 = LeafNode((0,0))
	# 	leafA_2 = LeafNode((0,0))
	# 	leafB_1 = LeafNode((0,1))
	# 	leafB_2 = LeafNode((0,1))
	# 	leafC_1 = LeafNode((1,0))
	# 	leafC_2 = LeafNode((1,0))
	# 	leafD_1 = LeafNode((1,1))
	# 	leafD_2 = LeafNode((1,1))
	# 	sum_1 = SumNode(leafA_1,leafA_2)
	# 	sum_2 = SumNode(leafB_1,leafB_2)
	# 	sum_3 = SumNode(leafC_1,leafC_2)
	# 	sum_4 = SumNode(leafD_1,leafD_2)
	# 	root = ProductNode(sum_1,sum_2,sum_3,sum_4)

	# 	spn = SumProductNetwork(root, input_shape=(2,2))

	# 	dataset = np.array([[[0.50223691, 0.15259684],
 #        [0.50609904, 0.37866967]],[[0.7114304 , 0.77245419],[0.11068035, 0.32439073]],[[0.33121913, 0.70182919],[0.2085118 , 0.47133778]],[[0.19511459, 0.59243772],[0.43258391, 0.73149234]],[[0.9428503 , 0.08890638],[0.20940068, 0.83670905]],[[0.24276965, 0.40897442],[0.66426178, 0.33020762]],[[0.09490107, 0.28950708],[0.09723734, 0.19489398]],[[0.42706855, 0.00578687],[0.39473137, 0.9192502 ]],[[0.66257775, 0.23560639],[0.84955456, 0.95110539]],[[0.47125085, 0.28107324],[0.7097553 , 0.126198  ]]])

	# 	leaf_leaner = PercentileGaussianLeafLearning(spn=spn,data=dataset,n_components=2)
	# 	leaf_leaner.fit()

	# 	spn.compile()
	# 	spn.fit(dataset, print_cost=0)

	# 	root_value = spn.inference(dataset)

	# 	self.assertTrue(np.allclose(root_value,[[-5.808628 ],[-5.556583 ],[-5.71042  ],[-5.4963517],[-5.43036  ],[-5.7175894],[-6.6184063],[-5.68887  ],[-4.858853 ],[-5.764823 ]]))

    def test_to_graph_sum_product(self):

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,0))
        leaf3 = InputLeafNode((0,1))
        leaf4 = InputLeafNode((0,1))
        prod1 = ProductNode(leaf1,leaf3)
        prod2 = ProductNode(leaf2,leaf4)
        sum1 = SumNode(prod1,prod2)
        spn = SumProductNetwork(sum1, input_shape=(1,2))
        g = spn.to_graph()
        #nx.drawing.nx_pydot.write_dot(g, "g.dot")
        
        all_graph_ids = [n for n in g.nodes if g.nodes[n] != None]
        #print('nodes: ', [n for n in g.nodes()])
        all_nodes_ids = []
        for node in spn.topological_sort():
            #all_nodes_ids.append(id(node))
            all_nodes_ids.append(node.id)
        
        #print('all_graph_ids: ', all_graph_ids)
        #print('all_nodes_ids: ', all_nodes_ids)
        self.assertEqual(all_graph_ids,all_nodes_ids)
    
    def test_to_graph_poon_domingos_1_3_1(self):
        
        learner = PoonAndDomingosStructuralLearning(input_shape=(1,3),leaf_channels=1)
        spn = learner.get_spn()
        g = spn.to_graph()
        #nx.drawing.nx_pydot.write_dot(g, "g.dot")

        all_graph_ids = [n for n in g.nodes()]
        all_nodes_ids = []
        for node in spn.topological_sort():
            #all_nodes_ids.append(id(node))
            all_nodes_ids.append(node.id)

        #print('all_graph_ids: ', all_graph_ids)
        #print('all_nodes_ids: ', all_nodes_ids)
        self.assertEqual(all_graph_ids,all_nodes_ids)
    
    def test_to_graph_poon_domingos_2_2_2(self):
            
        learner = PoonAndDomingosStructuralLearning(input_shape=(2,2),leaf_channels=2)
        spn = learner.get_spn()
        g = spn.to_graph()
        #nx.drawing.nx_pydot.write_dot(g, "g.dot")

        all_graph_ids = [n for n in g.nodes()]
        all_nodes_ids = []
        for node in spn.topological_sort():
            all_nodes_ids.append(node.id)

        #print('all_graph_ids: ', all_graph_ids)
        #print('all_nodes_ids: ', all_nodes_ids)
        self.assertEqual(all_graph_ids,all_nodes_ids)
    
    def test_to_graph_Conv2D_4_4_2(self):
        
        learner = Conv2DStructuralLearning(input_shape=(4,4),leaf_channels=2)
        spn = learner.get_spn()
        g = spn.to_graph()
        #nx.drawing.nx_pydot.write_dot(g, "g.dot")

        all_graph_ids = [n for n in g.nodes()]
        all_nodes_ids = []
        for node in spn.topological_sort():
            all_nodes_ids.append(node.id)

        #print('all_graph_ids: ', all_graph_ids)
        #print('all_nodes_ids: ', all_nodes_ids)
        self.assertEqual(all_graph_ids,all_nodes_ids)

#[TODO: normalize of LeafLearning]
class LeafLearningTest(unittest.TestCase):

    def test_normalize_1D(self):
        data = np.array([[[1,5]], [[2,5]], [[1,5]],[[4,5]]])
        # print('>>>>>>>>',data.shape)
        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))
        prod1 = ProductNode(leaf1,leaf2)

        spn = SumProductNetwork(prod1,input_shape=(1,2))

        leaf_leaner = LeafLearning(spn,data,1)

        norm_data = leaf_leaner.normalize(data)
        
        self.assertTrue(np.allclose(norm_data,[[[-0.81649658,  0.0]], [[ 0.0, 0.0]], [[-0.81649658,  0.0 ]], [[ 1.63299316,  0.0]]]))

    def test_normalize_2D(self):
        
        data = np.array([[[0.1, 0.2],[1.0, 0.9]], 
                        [[1.0, 0.4],[0.2, 0.9]], 
                        [[0.4, 0.9],[0.5, 0.9]]])

        leaf1 = InputLeafNode((0,0))
        leaf2 = InputLeafNode((0,1))
        leaf3 = InputLeafNode((1,0))
        leaf4 = InputLeafNode((1,1))

        root = ProductNode(leaf1,leaf2, leaf3, leaf4)
        spn = SumProductNetwork(root, input_shape=(2,2))

        leaf_leaner = LeafLearning(spn, data,1)

        norm_data = leaf_leaner.normalize(data)

        self.assertTrue(np.allclose(norm_data, [[[-1.06904497, -1.01904933], [ 1.31319831,  0.0]], [[ 1.33630621, -0.33968311],  [-1.1111678,   0.0]], [[-0.26726124,  1.35873244],  [-0.20203051,  0.0]]]  ))


class PercentileGaussianLeafLearningTest(unittest.TestCase):

    def test_compute_mpe_mask_2D(self):

        from lilspn.node import MARG_VAR_VAL
        
        # x = np.linspace(0.01, 0.99, 100)
        # print('x >>>>>> ', x)
        # mean = 0.5
        # std = 1
        
        # print('ppf >>>> ', norm.ppf(x, mean, std))
        # print('')
        
        # # print('mean: ', mean)
        # # print('var: ', var)
        # print('value 0.5: ', norm.ppf(0.5,mean, std))
        # print('value 0: ', norm.ppf(0.01,mean, std))
        # print('value 1: ', norm.ppf(0.99,mean, std))
        
        mean_l1 = 0.8; std1 = 1
        leaf1 = GaussianLeafNode(index_ph=(0,0), mean=mean_l1, std=std1)
        mean_l2 = 0.3; std2 = 1
        leaf2 = GaussianLeafNode(index_ph=(0,0), mean=mean_l2, std=std2)
        
        mean_l3 = 0.4; std3 = 1
        leaf3 = GaussianLeafNode(index_ph=(0,1), mean=mean_l3, std=std3)
        mean_l4 = 0.6; std4 = 1
        leaf4 = GaussianLeafNode(index_ph=(0,1), mean=mean_l4, std=std4)
        
        mean_l5 = 0.5; std5 = 1
        leaf5 = GaussianLeafNode(index_ph=(1,0), mean=mean_l5, std=std5)
        mean_l6 = 0.3; std6 = 1
        leaf6 = GaussianLeafNode(index_ph=(1,0), mean=mean_l6, std=std6)
        
        mean_l7 = 0.9; std7 = 1
        leaf7 = GaussianLeafNode(index_ph=(1,1), mean=mean_l7, std=std7)
        mean_l8 = 1.0; std8 = 1
        leaf8 = GaussianLeafNode(index_ph=(1,1), mean=mean_l8, std=std8)
        
        sum1 = SumNode(leaf1, leaf2)
        sum2 = SumNode(leaf3, leaf4)
        sum3 = SumNode(leaf5, leaf6)
        sum4 = SumNode(leaf7, leaf8)
        root = ProductNode(sum1,sum2,sum3,sum4)
        spn = SumProductNetwork(root,input_shape=(2,2))
        
        spn.set_sum_weights(sum1,[[0.5, 0.5]])
        spn.set_sum_weights(sum2,[[0.5, 0.5]])
        spn.set_sum_weights(sum3,[[0.5, 0.5]])
        spn.set_sum_weights(sum4,[[0.5, 0.5]])

        dataset = np.array([[[MARG_VAR_VAL, 0.2],[MARG_VAR_VAL, 0.8]], [[0.9, MARG_VAR_VAL],[0.2, MARG_VAR_VAL]], [[MARG_VAR_VAL, MARG_VAR_VAL],[0.5, 0.6]]])
        
        root_value = spn.inference(dataset)

        mpe_value_leaves = spn.mpe_inference(dataset)
        print('>>>>>>> shape: ', mpe_value_leaves.shape)
        print('mpe_value_leaves >>>>>>> value: \n', mpe_value_leaves)


        
        expected_output = np.array([[[norm.pdf(0.1,mean_l1,std1), norm.pdf(0.4,mean_l4,std4)],
                                     [norm.pdf(0.5,mean_l5,std5), norm.pdf(0.9,mean_l8,std8)]], 
                                    
                                    [[norm.pdf(0.3,mean_l2,std2), norm.pdf(0.6,mean_l4,std4)],
                                    [norm.pdf(0.3,mean_l6,std6), norm.pdf(1.0,mean_l8,std8)]]])
        print('>>>>>>> shape: ', expected_output.shape)
        print('pdfs >>>>>>> value: \n', expected_output)

        # self.assertTrue(np.allclose( mpe_value_leaves,expected_output ))

    #[TODO: fit of Percentile]
    # def test_fit_2D(self):
    #   raise Exception
    #   leafA_1 = LeafNode((0,0))
    #   leafA_2 = LeafNode((0,0))
    #   leafB_1 = LeafNode((0,1))
    #   leafB_2 = LeafNode((0,1))
    #   leafC_1 = LeafNode((1,0))
    #   leafC_2 = LeafNode((1,0))
    #   leafD_1 = LeafNode((1,1))
    #   leafD_2 = LeafNode((1,1))
    #   sum_1 = SumNode(leafA_1,leafA_2)
    #   sum_2 = SumNode(leafB_1,leafB_2)
    #   sum_3 = SumNode(leafC_1,leafC_2)
    #   sum_4 = SumNode(leafD_1,leafD_2)
    #   root = ProductNode(sum_1,sum_2,sum_3,sum_4)

    #   spn = SumProductNetwork(root, input_shape=(None,2,2))

    #   dataset = np.array([[[0.50223691, 0.15259684],
 #        [0.50609904, 0.37866967]],[[0.7114304 , 0.77245419],[0.11068035, 0.32439073]],[[0.33121913, 0.70182919],[0.2085118 , 0.47133778]],[[0.19511459, 0.59243772],[0.43258391, 0.73149234]],[[0.9428503 , 0.08890638],[0.20940068, 0.83670905]],[[0.24276965, 0.40897442],[0.66426178, 0.33020762]],[[0.09490107, 0.28950708],[0.09723734, 0.19489398]],[[0.42706855, 0.00578687],[0.39473137, 0.9192502 ]],[[0.66257775, 0.23560639],[0.84955456, 0.95110539]],[[0.47125085, 0.28107324],[0.7097553 , 0.126198  ]]])

    #   leaf_leaner = PercentileGaussianLeafLearning(spn=spn,data=dataset,n_components=2)
    #   leaf_leaner.fit()
        # print('leafA_1 means: ', sum_1.children[0].mean)
        # print('leafA_2:means: ', sum_1.children[1].mean)
        # print('leafB_1:means: ', sum_2.children[0].mean)
        # print('leafB_2:means: ', sum_2.children[1].mean)
        # print('leafC_1:means: ', sum_3.children[0].mean)
        # print('leafC_2:means: ', sum_3.children[1].mean)
        # print('leafD_1:means: ', sum_4.children[0].mean)
        # print('leafD_2:means: ', sum_4.children[1].mean)

if __name__ == '__main__':
    unittest.main()