import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

#to the function which creates the SPN given the graph g
import tensorflow as tf
import numpy as np
from lilspn.node import SumNode, ProductNode, LeafNode  #the correct is to get a LeafNode
#from node import SumNode, ProductNode, InputLeafNode #just to test inference later
from lilspn.spn import SumProductNetwork
#import .node

from collections import deque
#

from data_rectangle import DataRectangle

from collections import deque

def build_spn_graph(m,n,n_sum):
    """
    This function builds the sum-product network (SPN) structure as described in (Pedro Domingos, 2011)
    
    m: rectangle's number of rows 
    n: rectangle's number of columns
    n_sum: the number of sum nodes added per rectangle created(including the leaves)

    Return:
    g: the DAG 

    [TODO: ?A new class called DataCuts to store the relationship between the product nodes and one cut]
    [TODO: Save in the class DataRectangle which layer each rectangle belongs]
    [TODO: save the rectangles in a list or dict(id:rectangle) and return it]
    """

    n_prod = n_sum**2 #number of prod nodes added per cut
    #DAG
    g = nx.DiGraph()
    #identifier for all the SPN nodes (root id = 0)
    global_nodes_id = 0 
    #First rectangle: defining it as the root rectangle
    whole_rectangle = DataRectangle(np.arange(m),np.arange(n))
    #initial condition for the sum nodes ids associated to each rectangle: rootId -> first rectangle
    whole_rectangle.set_sum_nodes_ids(np.array([global_nodes_id]))
    #root is te first node
    g.add_node(global_nodes_id,type='root',label='+')
    
    #queue to store the rectangles to process
    rectangles_to_process = deque()
    rectangles_to_process.append(whole_rectangle)
    
    while rectangles_to_process:
        current_rectangle = rectangles_to_process.popleft()

        print('Processing rectangle id: ', current_rectangle.id)
        print('M: ', current_rectangle.rows_ids)
        print('N: ', current_rectangle.columns_ids)
        print('')
        
        #Vertical cuts
        if len(current_rectangle.columns_ids) > 1:
            for i in range(1,len(current_rectangle.columns_ids)):
                # The prod nodes
                prod_nodes_ids = np.arange(global_nodes_id+1,global_nodes_id+n_prod+1)
                #pointing global to the last id created
                global_nodes_id = prod_nodes_ids[len(prod_nodes_ids)-1]
                #creating prod nodes in graph
                g.add_nodes_from(prod_nodes_ids,type='product',label='*')

                #left sum nodes (they can be leaves)
                left_sum_nodes_ids = np.arange(global_nodes_id+1,global_nodes_id+n_sum+1)
                global_nodes_id = left_sum_nodes_ids[len(left_sum_nodes_ids)-1]
                            
                #right sum nodes (they can be leaves)
                right_sum_nodes_ids = np.arange(global_nodes_id+1,global_nodes_id+n_sum+1)
                global_nodes_id = right_sum_nodes_ids[len(right_sum_nodes_ids)-1]
                
                #left rectangle
                left_rec = DataRectangle(current_rectangle.rows_ids, current_rectangle.columns_ids[:i])
                #saving sum ids related to the rectangle
                left_rec.set_sum_nodes_ids(left_sum_nodes_ids)
                #creating left sum nodes in graph, or leaves
                if(len(left_rec.rows_ids)==1 and len(left_rec.columns_ids)==1): 
                    #g.add_nodes_from(left_sum_nodes_ids,type='leaf',m=left_rec.rows_ids,n=left_rec.columns_ids)#it is a leaf
                    g.add_nodes_from(left_sum_nodes_ids,type='leaf',scope=(left_rec.rows_ids[0],left_rec.columns_ids[0]),label="({},{})".format(left_rec.rows_ids[0],left_rec.columns_ids[0]))#it is a leaf
                else:
                    g.add_nodes_from(left_sum_nodes_ids,type='sum',label='+')#it is a sum nodes

                #right rectangle
                right_rec = DataRectangle(current_rectangle.rows_ids, current_rectangle.columns_ids[i:])
                #saving sum ids related to the rectangle[TODO: save the rectangles]
                right_rec.set_sum_nodes_ids(right_sum_nodes_ids)
                #creating right sum nodes in graph, or leaves
                if(len(right_rec.rows_ids)==1 and len(right_rec.columns_ids)==1): 
                    #g.add_nodes_from(right_sum_nodes_ids,type='leaf',m=right_rec.rows_ids,n=right_rec.columns_ids)#it is a leaf
                    g.add_nodes_from(right_sum_nodes_ids,type='leaf',scope=(right_rec.rows_ids[0],right_rec.columns_ids[0]),label="({},{})".format(right_rec.rows_ids[0],right_rec.columns_ids[0]))#it is a leaf
                else:
                    g.add_nodes_from(right_sum_nodes_ids,type='sum',label='+')#it is a sum node

                #-------------adding the edges------------
                    #from each of the current.sum_nodes_ids to each of the product nodes ids
                for i in current_rectangle.sum_nodes_ids:
                    for j in prod_nodes_ids:
                        g.add_edge(i,j)        
                        #print(g.edges[i][j])
                
                    #each product id gets one combination of left_ids and right_ids
                comb = [(x,y) for x in left_sum_nodes_ids for y in right_sum_nodes_ids]
                for i,c in zip(prod_nodes_ids,comb):
                        g.add_edge(i,c[0])
                        g.add_edge(i,c[1])
                
                #adding the new rectangles to the queue
                rectangles_to_process.append(left_rec)
                rectangles_to_process.append(right_rec)
        # Horizontal cuts
        if len(current_rectangle.rows_ids) > 1:
            for i in range(1,len(current_rectangle.rows_ids)):
                # The prod nodes
                prod_nodes_ids = np.arange(global_nodes_id+1,global_nodes_id+n_prod+1)
                #pointing global to the last id created
                global_nodes_id = prod_nodes_ids[len(prod_nodes_ids)-1]
                #creating prod nodes in graph
                g.add_nodes_from(prod_nodes_ids,type='product',label='*')

                #left sum nodes
                left_sum_nodes_ids = np.arange(global_nodes_id+1,global_nodes_id+n_sum+1)
                global_nodes_id = left_sum_nodes_ids[len(left_sum_nodes_ids)-1]
                
                #right sum nodes
                right_sum_nodes_ids = np.arange(global_nodes_id+1,global_nodes_id+n_sum+1)
                global_nodes_id = right_sum_nodes_ids[len(right_sum_nodes_ids)-1]

                #upper rectangle
                upper_rec = DataRectangle(current_rectangle.rows_ids[:i], current_rectangle.columns_ids)
                #saving sum ids related to the rectangle
                upper_rec.set_sum_nodes_ids(left_sum_nodes_ids)
                #creating left sum nodes in graph, or leaves
                if(len(upper_rec.rows_ids)==1 and len(upper_rec.columns_ids)==1): 
                    #g.add_nodes_from(left_sum_nodes_ids,type='leaf',m=upper_rec.rows_ids,n=upper_rec.columns_ids)#it is a leaf
                    g.add_nodes_from(left_sum_nodes_ids,type='leaf',scope=(upper_rec.rows_ids[0],upper_rec.columns_ids[0]),label="({},{})".format(upper_rec.rows_ids[0],upper_rec.columns_ids[0]))#it is a leaf
                else:
                    g.add_nodes_from(left_sum_nodes_ids,type='sum',label='+')#it is a sum nodes
                
                #bottom rectangle
                bottom_rec = DataRectangle(current_rectangle.rows_ids[i:], current_rectangle.columns_ids)
                #saving sum ids related to the rectangle[TODO: save the rectangles]
                bottom_rec.set_sum_nodes_ids(right_sum_nodes_ids)
                #creating right sum nodes in graph, or leaves
                if(len(bottom_rec.rows_ids)==1 and len(bottom_rec.columns_ids)==1): 
                    g.add_nodes_from(right_sum_nodes_ids,type='leaf',scope=(bottom_rec.rows_ids[0],bottom_rec.columns_ids[0]),label="({},{})".format(bottom_rec.rows_ids[0],bottom_rec.columns_ids[0]))#it is a leaf
                else:
                    g.add_nodes_from(right_sum_nodes_ids,type='sum',label='+')#it is a sum node
                
                #-------------adding the edges------------
                    #from each of the current.sum_nodes_ids to each of the product nodes ids
                for i in current_rectangle.sum_nodes_ids:
                    for j in prod_nodes_ids:
                        g.add_edge(i,j)
                
                    #each product id gets one combination of left_ids and right_ids
                comb = [(x,y) for x in left_sum_nodes_ids for y in right_sum_nodes_ids]
                for i,c in zip(prod_nodes_ids,comb):
                        g.add_edge(i,c[0])
                        g.add_edge(i,c[1])
                
                #adding the new rectangle to the queue
                rectangles_to_process.append(upper_rec)
                rectangles_to_process.append(bottom_rec)
                
        # Leaf
        if(len(current_rectangle.rows_ids)==1 and len(current_rectangle.columns_ids)==1): 
            print('Leaf in m=', current_rectangle.rows_ids,' and n=',current_rectangle.columns_ids)
            print('')

    return g

def draw_graph(g):
    """
    This function draws the graph
    """
    root_node = [n for n in g.nodes() if g.in_degree(n) == 0]
    sum_nodes = [n for n in g.nodes() if g.nodes[n]['type'] == 'sum']
    prod_nodes = [n for n in g.nodes() if g.nodes[n]['type'] == 'product']
    leaf_nodes = [n for n in g.nodes() if g.out_degree(n) == 0]
    
    nx.draw_networkx(g,pos=nx.kamada_kawai_layout(g),with_labels=True,nodelist=list(root_node),edgelist=g.edges(),node_color='k')
    nx.draw_networkx(g,pos=nx.kamada_kawai_layout(g),with_labels=True,nodelist=list(sum_nodes),edgelist=g.edges(),node_color='r')
    nx.draw_networkx(g,pos=nx.kamada_kawai_layout(g),with_labels=True,nodelist=list(prod_nodes),edgelist=g.edges(),node_color='b')
    nx.draw_networkx(g,pos=nx.kamada_kawai_layout(g),with_labels=True,nodelist=list(leaf_nodes),edgelist=g.edges(),node_color='y')
    plt.draw()
    plt.show()

def print_graph_ids(g):
    """
    This function prints the ids of the root, sum, prod and leaf nodes.
    """
    print('------------Types of nodes with its ids------------')
    root_node = [n for n in g.nodes() if g.in_degree(n) == 0]
    print('root id: ', root_node)
    sum_nodes = [n for n in g.nodes() if g.nodes[n]['type'] == 'sum']
    print('sum nodes ids:', sum_nodes)
    prod_nodes = [n for n in g.nodes() if g.nodes[n]['type'] == 'product']
    print('prod nodes ids: ', prod_nodes)
    leaf_nodes = [n for n in g.nodes() if g.out_degree(n) == 0]
    print('leaf nodes ids: ',leaf_nodes)
    print('Number of SumNodes: ', len(sum_nodes))
    print('Number of ProdNodes: ', len(prod_nodes))
    print('Number of LeafNodes: ', len(leaf_nodes))
    print('------------------------END------------------------')

def build_spn(graph,m,n):#GENERAL FUNCTION
    """
    This function builds the SPN using lil-spn given a graph(networkx)
    """
    all_nodes_ids = [n for n in graph.nodes()]
    print('all nodes ids: ', all_nodes_ids)
    
    #root_id = [n for n in graph.nodes() if graph.nodes[n]['type'] == 'root']
    root_id = [n for n in graph.nodes() if graph.in_degree(n) == 0]
    #root_id = root_id[0]
    #print('IDDDDDDDD:', root_id)
    #root_id = 0
    #leaves_ids = [n for n in graph.nodes() if graph.nodes[n]['type'] == 'leaf']
    leaves_ids = [n for n in graph.nodes() if graph.out_degree(n) == 0]
    print('leaves_ids: ', leaves_ids)
    sum_nodes_ids = [n for n in graph.nodes() if graph.nodes[n]['type'] == 'sum']
    print('sum nodes_ids: ',sum_nodes_ids)
    prod_nodes_ids = [n for n in graph.nodes() if graph.nodes[n]['type'] == 'product']
    print('prod nodes_ids: ',prod_nodes_ids)
    print('')

    # leaves_graph = [leaf for leaf in graph.nodes() if graph.out_degree(leaf) == 0]
    # print(leaves_graph)
    # print(leaves_graph[0])
    
    #dicts of lil_spn objects    
    nodes_dict = {}
    leaves_dict = {}
    sum_nodes_dict = {}
    prod_nodes_dict = {}


    #LEAVES_list
    #leaf_inputs = tf.placeholder(tf.float32, shape=(None,m,n))
    for l_id in leaves_ids:
        #print('id: ',l_id,'tuple: ',(graph.nodes[l_id]['m'][0],graph.nodes[l_id]['n'][0]))
        #leaf = InputLeafNode(leaf_inputs,(graph.nodes[l_id]['m'][0],graph.nodes[l_id]['n'][0]))
        
        #leaf = InputLeafNode((graph.nodes[l_id]['m'][0],graph.nodes[l_id]['n'][0]))
        leaf = LeafNode((graph.nodes[l_id]['scope'][0],graph.nodes[l_id]['scope'][1]))
        # print('scope m:', graph.nodes[l_id]['scope'][0], '   scope n: ', graph.nodes[l_id]['scope'][1])
        # print('scope:', graph.nodes[l_id]['scope'])
        
        #leaves_list.append(leaf)
        leaves_dict[l_id] = leaf
        nodes_dict[l_id] = leaf
    # #print(leaves_list)
    print('nodes_dict(only leaves): ',nodes_dict)
    
    #nodes_to_process = leaves_ids
    #stack to stock the nodes to process id and its successor in list, starting with the root
    nodes_to_process = deque()
    nodes_to_process.extendleft(root_id)#the list should have only one value(root)
    node_children_dict = {}

    #queue aux to create the stack
    queue_finding_clilds = deque()
    queue_finding_clilds.extendleft(root_id)
    # print('nodes_to_process: ', nodes_to_process)
    # print('queue_finding_clilds: ', queue_finding_clilds)
    # print('')
    
    #first case: root

    while queue_finding_clilds:
        node_id = queue_finding_clilds.popleft()
        
        successors = list(graph.successors(node_id))
        #print('successor id ', node_id, ': ' ,successors)
        
        #stock in a dict the node_id with its children
        node_children_dict[node_id] = successors
        
        #adding the children to the queue and to the stack if they aren't there
        for successor in successors:
            #adding to the stack to process later
            if not(successor in nodes_to_process):
                nodes_to_process.append(successor)
            #adding to the queue to continue finding children
            queue_finding_clilds.append(successor)
        #print('nodes_to_process: ', nodes_to_process)
        #print('queue_finding_clilds: ', queue_finding_clilds)
        #print('')
    #print('nodes_to_process: ', nodes_to_process)
    
    #nodes with its children
    #for node_id in all_nodes_ids:
        # print('node_id: ', node_id)
        # print('successors: ',node_children_dict[node_id])
        # print('')
    #final stack with the nodes_ids to process in a safe order
    print('nodes_to_process: ', nodes_to_process)
        
    while(nodes_to_process):
        # node_id = nodes_to_process[0]
        # nodes_to_process.remove(node_id)
        node_id = nodes_to_process.pop()
        #print('nodes_to_process: ', nodes_to_process)
        print('')
        print('Processing node_id: ',node_id)
        #if node_id == 0:
        #    raise Exception
        #if graph.nodes[node_id]['type'] == 'root':
        if graph.in_degree(node_id) == 0:
            print('RootNode children:', node_children_dict[node_id])
            lil_spn_children = []
            for child_id in node_children_dict[node_id]:
                lil_spn_children.append(nodes_dict[child_id])
            print('lil_spn_root_children: ', lil_spn_children)
            #root = SumNode(node_children_dict[node_id])
            root = SumNode(*lil_spn_children)
            nodes_dict[node_id] = SumNode(*lil_spn_children)        
        elif graph.nodes[node_id]['type'] == 'sum':
            print('SumNode children:', node_children_dict[node_id])
            lil_spn_children = []
            for child_id in node_children_dict[node_id]:
                lil_spn_children.append(nodes_dict[child_id])
            print('lil_spn_Sum_children: ', lil_spn_children)
            #sum_nodes_dict[node_id] = SumNode(node_children_dict[node_id])
            sum_nodes_dict[node_id] = SumNode(*lil_spn_children)
            nodes_dict[node_id] = SumNode(*lil_spn_children)    

        elif graph.nodes[node_id]['type'] == 'product':
            print('ProdNode children:', node_children_dict[node_id])
            #getting lil-spn objects
            lil_spn_children = []
            for child_id in node_children_dict[node_id]:
                lil_spn_children.append(nodes_dict[child_id])
            print('lil_spn_Prod_children: ', lil_spn_children)
            #prod_nodes_dict[node_id] = ProductNode(lil_spn_children)    
            prod_nodes_dict[node_id] = ProductNode(*lil_spn_children)    
            nodes_dict[node_id] = ProductNode(*lil_spn_children)    
        #it's a leaf
        elif(len(node_children_dict[node_id]) == 0):
            print('leaf: ', nodes_dict[node_id])
    print('')
    print('final dict with lil_spn objects: ',nodes_dict)
    return nodes_dict
#GRAFICO GERADO DIRETAMENTE COM A FUNCAO BUILD_GRAPH
def gen_dot_poon_and_domingos():
    #case 1x2(1)
    m = 1; n = 2; n_sum = 1
    graph = build_spn_graph(m,n,n_sum)
    print_graph_ids(graph)
    #draw_graph(graph)
    spn = build_spn(graph,m,n)
    nx.drawing.nx_pydot.write_dot(graph, "materials/poon_and_domingos_graphs/g_1_2_1.dot")
    #case 1x3(1)
    m = 1; n = 3; n_sum = 1
    graph = build_spn_graph(m,n,n_sum)
    print_graph_ids(graph)
    #draw_graph(graph)
    spn = build_spn(graph,m,n)
    nx.drawing.nx_pydot.write_dot(graph, "materials/poon_and_domingos_graphs/g_1_3_1.dot")
    #case 1x4(1)
    m = 1; n = 4; n_sum = 1
    graph = build_spn_graph(m,n,n_sum)
    print_graph_ids(graph)
    #draw_graph(graph)
    nx.drawing.nx_pydot.write_dot(graph, "materials/poon_and_domingos_graphs/g_1_4_1.dot")
    #case 2x2(1)
    m = 2; n = 2; n_sum = 1
    graph = build_spn_graph(m,n,n_sum)
    print_graph_ids(graph)
    #draw_graph(graph)
    nx.drawing.nx_pydot.write_dot(graph, "materials/poon_and_domingos_graphs/g_2_2_1.dot")
    #case 2x2(2)
    m = 2; n = 2; n_sum = 2
    graph = build_spn_graph(m,n,n_sum)
    print_graph_ids(graph)
    #draw_graph(graph)
    nx.drawing.nx_pydot.write_dot(graph, "materials/poon_and_domingos_graphs/g_2_2_2.dot")
    #case 2x3(1)
    m = 2; n = 3; n_sum = 1
    graph = build_spn_graph(m,n,n_sum)
    print_graph_ids(graph)
    #draw_graph(graph)
    nx.drawing.nx_pydot.write_dot(graph, "materials/poon_and_domingos_graphs/g_2_3_1.dot")
    #case 3x3(1)
    m = 3; n = 3; n_sum = 1
    graph = build_spn_graph(m,n,n_sum)
    print_graph_ids(graph)
    nx.drawing.nx_pydot.write_dot(graph, "materials/poon_and_domingos_graphs/g_3_3_1.dot")
    #draw_graph(graph)
    # #case 4x4(1)
    m = 4; n = 4; n_sum = 1
    graph = build_spn_graph(m,n,n_sum)
    print_graph_ids(graph)
    #draw_graph(graph)
    nx.drawing.nx_pydot.write_dot(graph, "materials/poon_and_domingos_graphs/g_4_4_1.dot")
    # #case 6x6(1)
    # # m = 6; n = 6; n_sum = 1
    # # graph = build_spn_graph(m,n,n_sum)
    # # print_graph_ids(graph)

#GERAR GRAFICO COM A FUNCAO DA PROPRIA CONV2d
def gen_dot_conv2D():
    #1 4 2
    learner = Conv2DStructuralLearning(input_size=(1,4),leaf_channels=2)
    G = learner.get_structure()
    #G = learner.graph
    spn = learner.get_spn()
    nx.drawing.nx_pydot.write_dot(G, "materials/conv2D_graphs/g_1_4_2.dot")
    #1 5 2
    learner = Conv2DStructuralLearning(input_size=(1,5),leaf_channels=2)
    G = learner.get_structure()
    #G = learner.graph
    spn = learner.get_spn()
    nx.drawing.nx_pydot.write_dot(G, "materials/conv2D_graphs/g_1_5_2.dot")

    #4 4 2
    learner = Conv2DStructuralLearning(input_size=(4,4),leaf_channels=2)
    G = learner.get_structure()
    #G = learner.graph
    spn = learner.get_spn()
    nx.drawing.nx_pydot.write_dot(G, "materials/conv2D_graphs/g_4_4_2.dot")

    #3 3 2
    learner = Conv2DStructuralLearning(input_size=(3,3),leaf_channels=2)
    G = learner.get_structure()
    #G = learner.graph
    spn = learner.get_spn()
    nx.drawing.nx_pydot.write_dot(G, "materials/conv2D_graphs/g_3_3_2.dot")

def main():
    print('main')
    #example
#     leaf_inputs = tf.placeholder(tf.float32, shape=(None,2))
#     leaf1 = InputLeafNode((0))
#     leaf2 = InputLeafNode(1)
#     prod1 = ProductNode(leaf1,leaf2)
#     root1 = SumNode(prod1)
#     spn1 = SumProductNetwork(root1)
#     #setting placeholders
#     spn1.set_input(leaf_inputs)

#     print('root1: ', root1)
#     print('root1 type: ', type(root1))
#     with tf.Session() as sess:
#             assign_op = tf.assign(
#                 root1.weights, 
#                 tf.convert_to_tensor([[0.7]])
#                 )

#             sess.run(assign_op)

#             dataset = [[0.1, 0.2], [1.0, 0.4], [0.5, 0.9]]

#             root1_value = sess.run(root1.get_value(),feed_dict={leaf_inputs:dataset})
#             print('root1_value: ', root1_value)#[[-3.912023],[-0.9162907],[-0.79850775]]

#             print('')
#             print('')
#             print('')
# #ln(0.1)+ln(0.2)*(0.7/0.7) =
# #(−2.302585093−1.609437912)*(0.7/0.7)=−3.912023005

    # #my code
    # m = 1; n = 2; n_sum = 1
    # g = build_spn_graph(m,n,n_sum)
    # print_graph_ids(g)
    # #draw_graph(g)
    # #top(g)
    
    # spn = build_spn(g,m,n)
    # root = spn[0]
    # #setting placeholders
    # leaf_inputs2 = tf.placeholder(tf.float32, shape=(None,2))
    # spn2 = SumProductNetwork(root)
    # #setting placeholders
    # spn2.set_input(leaf_inputs2)
    
    
    # print('root: ', root)
    # print('root type: ',type(root))
    
    # #print('spn: ',spn)
    # # for s in spn:
    # #     print('spn: ', s)
    # #     print('')


    # with tf.Session() as sess:
    #         assign_op = tf.assign(
    #             root.weights, 
    #             tf.convert_to_tensor([[0.7]])
    #             )

    #         sess.run(assign_op)

    #         dataset = [[0.1, 0.2], [1.0, 0.4], [0.5, 0.9]]

    #         root_value = sess.run(root.get_value(),feed_dict={leaf_inputs2:dataset})

    #         #self.assertTrue(np.allclose(root_value,[[-0.58789784], [-1.16532862], [-0.32568729]]))
    #         print('root value: ',root_value)
    #         #[[-0.58789784], [-1.16532862], [-0.32568729]]


    



    # #---------- create unity tests

    

if __name__== "__main__":
    main()
    #gen_dot_poon_and_domingos()
    #gen_dot_conv2D()
