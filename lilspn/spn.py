import tensorflow as tf
import networkx as nx

from lilspn.utilities import random_mini_batches
from lilspn.node import LeafNode, SumNode, ProductNode
from lilspn.node import InputLeafNode
from itertools import combinations
from collections import deque
from lilspn.utilities import EPSILON


class SumProductNetwork:


    def __init__(self, root_node, input_shape, _nodes_dict=None, _graph=None, seed=1234):
        if  input_shape is None or len(input_shape) != 2:
            raise ValueError("Input shape for SPN must be of length 2 and format (m,n)")

        # setup
        self.input_shape = input_shape
        self.root_node = root_node

        # NetworkX connection
        self.nodes_dict = _nodes_dict
        self.graph = _graph
        self.setup()

        # Tensorflow setup
        self.tf_graph = tf.Graph()
        self.set_seed(seed)
        with self.tf_graph.as_default():
            self.spn_input = tf.placeholder(tf.float32, shape=(None,self.input_shape[0],self.input_shape[1]))
            self.sess = tf.Session(graph=self.tf_graph)
            self.set_input(self.spn_input)


    def set_seed(self, seed):
        self.seed = seed
        with self.tf_graph.as_default():
            tf.set_random_seed(seed)


    def setup(self):

        self.node_type_mapping = {'leaf': [self.nodes_dict[node_id] for node_id in self.graph.nodes() if self.graph.out_degree(node_id) == 0],
                             'sum': [self.nodes_dict[node_id] for node_id in self.graph.nodes() if self.graph.nodes[node_id]['type'] == 'sum'], 
                             'product': [self.nodes_dict[node_id] for node_id in self.graph.nodes() if self.graph.nodes[node_id]['type'] == 'product'],
                             'root': [self.nodes_dict[node_id] for node_id in self.graph.nodes() if self.graph.in_degree(node_id) == 0]
                             }
        self.topological_sort_nodes = [self.nodes_dict[n] for n in nx.topological_sort(self.graph)]


    def initialize_weights(self):
        with self.tf_graph.as_default():
            init = tf.global_variables_initializer()
        self.sess.run(init)


    def inference(self, data):
        root_value = self.sess.run(self.root_node.value,feed_dict={self.spn_input:data})
        return root_value


    def mpe_inference(self,data):
        mpe_solution = self.sess.run(self.mpe_assignments,feed_dict={self.spn_input:data})
        return mpe_solution


    def set_sum_weights(self, sum_node, weights):

        if not isinstance(sum_node,SumNode):
            raise ValueError("Sum node only")

        with self.tf_graph.as_default():
            assign_op = tf.assign(sum_node.weights, tf.convert_to_tensor(weights))

        self.sess.run(assign_op)


    def build_forward(self):

        with self.tf_graph.as_default():

            top_sort_rev = self.topological_sort(reverse=True)

            for curr_node in top_sort_rev:
                num_ch = len(curr_node.children)
                children_values = None
                if num_ch == 1:
                    children_values = curr_node.children[0].value
                elif num_ch > 1:
                    children_values = tf.concat([child.value for child in curr_node.children],axis=1)
                curr_node.value = curr_node.compute_value(children_values)


    def build_backward_masks(self):
        # The MPE mask is a 0 or 1 to indicate if current node
        # is still participating in the ongoing MPE evaluation
        # Returns shape (N x V), where V is the number of children
        with self.tf_graph.as_default():

            # Parent's mask for root node is 1
            parents_masks = { self.root_node.id: tf.ones(shape=(tf.shape(self.root_node.value)[0],1), dtype=tf.float32) }

            for curr_node in self.topological_sort():
                # Prepare Mask from Parents
                sum_parents_masks = tf.reduce_sum(parents_masks[curr_node.id],axis=1,keepdims=True)
                curr_node.mpe_mask = tf.where(tf.less(sum_parents_masks, EPSILON), sum_parents_masks, sum_parents_masks / sum_parents_masks)
                curr_node.mpe_mask = tf.stop_gradient(curr_node.mpe_mask)

                # Compute children mask
                if len(curr_node.children) > 0:
                    possible_children_mask =  curr_node.compute_children_mpe_mask() # Shape (N x V)
                    possible_children_mask = tf.stop_gradient(possible_children_mask)
                    children_mask = tf.multiply(curr_node.mpe_mask, possible_children_mask) # Shape (N x V)
                    for i,child in enumerate(curr_node.children):
                        if child.id not in parents_masks:
                            parents_masks[child.id] = tf.reshape(children_mask[:,i], shape=(-1,1))
                        else:
                            parents_masks[child.id] = tf.concat([parents_masks[child.id],tf.reshape(children_mask[:,i], shape=(-1,1))],axis=1)


    def build_mpe_assignments(self):
        with self.tf_graph.as_default():
            # Propagating Masks
            self.build_backward_masks()

            # Given masks, compute MPE value for leaf nodes
            mpe_leaves = {}
            for leaf in self.get_nodes_by_type("leaf"):
                possible_mpe_value = leaf.compute_mpe_value() # Shape (N x 1)
                leaf.mpe_value = tf.multiply(leaf.mpe_mask, possible_mpe_value)
                if leaf.scope not in mpe_leaves:
                    mpe_leaves[leaf.scope] = leaf.mpe_value
                else:
                    # For each leaf variable, sum all of its MPE values
                    # Since only 1 leaf variable should have a state (the other ones are zero),
                    # the sum yields the state of the leaf variable itself.
                    mpe_leaves[leaf.scope] = tf.reduce_sum(tf.concat([mpe_leaves[leaf.scope], leaf.mpe_value],axis=1), axis=1, keepdims=True)

            # return a tensor using the position in the input place-hold, which is the scope
            # mpe_out_shape = (batch_amt,self.input_shape[0],self.input_shape[1])
            rolled_mpe_output = tf.concat([mpe_leaves[pos] for pos in sorted(mpe_leaves.keys())], axis=1)

            # # Reshape output
            self.mpe_assignments = tf.reshape(rolled_mpe_output, shape=(-1,self.input_shape[0],self.input_shape[1]))


    def set_input(self,placeholder):
        with self.tf_graph.as_default():
            for node in self.get_nodes_by_type("leaf"):
                node.set_input(placeholder)


    def get_nodes_by_type(self,node_type):
        if node_type not in self.node_type_mapping:
            raise ValueError("Type non-existent")

        return self.node_type_mapping[node_type]


    def topological_sort(self, reverse=False):
        return self.topological_sort_nodes if not reverse else reversed(self.topological_sort_nodes)


    def is_valid(self):
        counter_no_completeness = 0 # number of sum nodes whose children don't have the same scope
        counter_no_decomposable = 0 # number of product nodes whose children don't have different scope
        dict_nodes_scope = {}
        
        #topological
        nodes = self.topological_sort(reverse=True)

        for node in nodes:
            if isinstance(node,LeafNode):
                node_scope_set = set()
                node_scope_set.add(node.scope)
                dict_nodes_scope[node.id] = node_scope_set #this node id as key, a list of tuples as value: leaf scope list
            else:
                if isinstance(node,SumNode): #is sum node
                    node_scope_set = set()
                    if len(node.children)==1:#case when node has only one child
                        node_scope_set = dict_nodes_scope[node.children[0].id]
                        dict_nodes_scope[node.id] = node_scope_set
                    else:
                        for (child1,child2) in combinations(node.children,r=2): #more than one child, use combinations(pairwise)
                            #child_set_scope = dict_nodes_scope[child.id]
                            child1_set_scope = dict_nodes_scope[child1.id]
                            child2_set_scope = dict_nodes_scope[child2.id]
                            node_scope_set = node_scope_set.union(node_scope_set,child1_set_scope.union(child2_set_scope))
                            if child2_set_scope != child1_set_scope: #if set aren't equal,then it's not complete
                                counter_no_completeness+=1 #this node has repeated scope tuple
                                
                        dict_nodes_scope[node.id] = node_scope_set

                elif isinstance(node,ProductNode): #is product node
                    node_scope_set = set()
                    if len(node.children)==1:#case when node has only one child
                        node_scope_set = dict_nodes_scope[node.children[0].id]
                        dict_nodes_scope[node.id] = node_scope_set
                    else:
                        for (child1,child2) in combinations(node.children,r=2):
                            #child_set_scope = dict_nodes_scope[child.id]
                            child1_set_scope = dict_nodes_scope[child1.id]
                            child2_set_scope = dict_nodes_scope[child2.id]
                            node_scope_set = node_scope_set.union(node_scope_set,child1_set_scope.union(child2_set_scope))
                            if child2_set_scope.intersection(child1_set_scope): #if there are elements in the intersection it's not decomposable
                                counter_no_decomposable+=1 #this node has repeated scope tuple
                            
                        dict_nodes_scope[node.id] = node_scope_set

                else:
                    raise ValueError('Node is not leaf, nor sum node, nor product node')

        if counter_no_decomposable == 0 and counter_no_completeness == 0:
            return True
        else:
            return False


    def build(self):
        with self.tf_graph.as_default():
            for node in self.topological_sort():
                node.build()


    def compile(self):
        with self.tf_graph.as_default():
            self.build()
            self.build_forward()
            self.loss = -1.0 * tf.reduce_mean(self.root_node.value)


    def compile_mpe(self):
        with self.tf_graph.as_default():
            # Build tf graph
            self.build()
            self.build_forward()
            self.build_mpe_assignments()

            # Labels input
            self.spn_input_labels = tf.placeholder(tf.float32, shape=(None,self.input_shape[0],self.input_shape[1]))

            # Loss function
            self.loss = tf.mean_squared_error(self.spn_input_labels, self.mpe_assignments)


    def fit(self, train_data, train_labels=None, num_epochs=50, minibatch_size=None, learning_rate=0.001, print_cost=1, logger_name="logger", track_memory=False):

        import logging
        import lilspn.mem_util

        with self.tf_graph.as_default():

            logger = logging.getLogger(logger_name)
            logger.setLevel(logging.DEBUG)
            logger.info("Fitting: start")

            # Memory track
            mem_track = {}
            if track_memory:
                run_metadata = tf.RunMetadata()
                mem_track['options'] = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                mem_track['run_metadata'] = run_metadata

            # Settings
            num_instances = train_data.shape[0]
            if not minibatch_size:
                _poss_size = int(num_instances / 4)
                minibatch_size = _poss_size if _poss_size > 0 else num_instances
                minibatch_size = minibatch_size if minibatch_size < 64 else 64
            
            # Optimizer
            logger.info("Fitting: loading optimizer")
            adam = tf.train.AdamOptimizer(learning_rate=learning_rate)
            logger.info("Fitting: optimizer minimize (computing and applying gradients)")
            self.optimizer = adam.minimize(loss=self.loss)

            # Initialization
            logger.info("Fitting: Initializing weights")
            self.initialize_weights()
            costs = []
            _seed = self.seed

            # Do the training loop
            for epoch in range(num_epochs):

                logger.info("Fitting: running epoch {}".format(epoch))

                minibatch_cost = 0.
                # number of minibatches of size minibatch_size in the train set
                num_minibatches = int(num_instances / minibatch_size)
                _seed += 1
                minibatches = random_mini_batches(train_data, train_labels, minibatch_size, _seed)

                for i, minibatch in enumerate(minibatches):
                    # Run the session to execute the optimizer and the cost, the feedict should contain a minibatch for (train_data,Y).
                    minibatch_X, minibatch_Y = minibatch
                    logger.info("Fitting: running minibatch {} in epoch {} with shape {}".format(i, epoch, minibatch_X.shape))

                    _feed_dict = {self.spn_input: minibatch_X}
                    if minibatch_Y:
                        _feed_dict[self.spn_input_labels] = minibatch_Y
                    _, temp_cost = self.sess.run([self.optimizer, self.loss], feed_dict=_feed_dict, **mem_track)
                    
                    minibatch_cost += temp_cost / num_minibatches
                    logger.info("Fitting: cost computed for minibatch {} in epoch {}".format(i, epoch))
                # Print the cost every n epoch
                if print_cost and (epoch % print_cost == 0):
                    print('{"metric": "LogLikelihood", "value": %f}' % (minibatch_cost))
                costs.append(minibatch_cost)
                logger.info("Fitting: cost computed for epoch {}".format(epoch))

            # Print memory tracking
            if track_memory:
                lilspn.mem_util.print_memory_timeline(run_metadata, to_file=track_memory)

            return costs


    def to_tensorboard(self, output_dir):
        with self.tf_graph.as_default():
            merged = tf.summary.merge_all()
            writer = tf.summary.FileWriter(output_dir,self.sess.graph)
            summary = self.sess.run(merged)
            writer.add_summary(summary)


    def to_graph(self, show_node_name=True, show_class_name=False, show_id=False):
        graph = nx.DiGraph()

        node_id_obj = None
        for node in self.topological_sort():
            label = []
            if show_class_name:
                label.append("'"+str(type(node).__name__)+"'")
            if show_id:
                label.append(str(node.id))
            node_id_obj = node.id
            if isinstance(node,LeafNode):
                if show_node_name:
                    label = [str(node.scope)] + label
                graph.add_node(node_id_obj,type='leaf',scope=node.scope, leaf_class=node.__class__.__name__ ,label=",".join(label))
            elif isinstance(node,SumNode):
                if show_node_name:
                    label = ["+"] + label
                graph.add_node(node_id_obj,type='sum',label=','.join(label))
                for child in node.children:
                    graph.add_edge(node_id_obj, child.id)
            elif isinstance(node,ProductNode):
                if show_node_name:
                    label = ["*"] + label
                graph.add_node(node_id_obj,type='product',label=','.join(label))
                for child in node.children:
                    graph.add_edge(node_id_obj, child.id)
            
        return graph



    def reset_node_states(self):
        # Can be used if multiple inference are done in sequence.
        # Since internal values are saved for efficiency, it is a good idea to
        # reset them to their default value (None) before performing new inference.
        for node in self.topological_sort():
            node.reset_state()


    @staticmethod
    def save(spn, save_path):

        # Save tensorflow graph
        with spn.tf_graph.as_default():
            saver = tf.train.Saver()
            saver.save(spn.sess, "{}/tf_model.ckpt".format(save_path))

        # Save SPN graph
        import networkx as nx
        graph = spn.to_graph()
        nx.drawing.nx_pydot.write_dot(graph,"{}/graph_model.dot".format(save_path))

        # Save extra information
        # info_file = open("{}/info_model.spn".format(save_path), "w")
        # info_file.write(",".join([str(n) for n in self.input_shape]) + "\n")
        # info_file.close()


    @staticmethod
    def load(save_path, input_shape):

        # Load extra information
        # info_file = open("{}/info_model.spn".format(save_path), "r")
        # in_shape = info_file.readline()
        # in_shape = tuple([int(n) for n in in_shape.split(",")])

        # Load SPN graph
        graph = nx.drawing.nx_pydot.read_dot("{}/graph_model.dot".format(save_path))
        # Fix scope string which comes with double string formatting, e.g., '"(0,1)"''
        # TODO: maybe make this in a better way
        for node in graph.nodes():
            if graph.nodes[node]["type"] == "leaf":
                graph.nodes[node]["scope"] = tuple([int(n) for n in graph.nodes[node]["scope"].replace("\"","").replace("(","").replace(")","").split(",")])
        # tf.reset_default_graph()

        from lilspn.learning import StructuralLearning

        learner = StructuralLearning(input_shape=input_shape, graph=graph)
        spn = learner.get_spn()
        spn.compile()

        # Load tensorflow graph
        with spn.tf_graph.as_default():
            # Load tensorflow graph
            saver = tf.train.Saver()
            saver.restore(spn.sess, "{}/tf_model.ckpt".format(save_path))

        # # Save new session
        # spn.sess = new_sess

        return spn
