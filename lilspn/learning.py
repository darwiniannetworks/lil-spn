import networkx as nx
import numpy as np
from collections import deque

from lilspn.node import SumNode, ProductNode, LeafNode, GaussianLeafNode
from lilspn.spn import SumProductNetwork
from lilspn.utilities import DataRectangle
from lilspn.utilities import Region

# -------------------
# Structure Learning
# -------------------

class StructuralLearning:
    """
    Base class for the different methods to learn the structure of a SPN.
    A StructuralLearning stores the learned SPN graph using a Digraph Networkx object.
    Warning: before using a StructuralLearning object method, set its graph.
    Parameters
    ----------
    
    See Also
    --------
    Conv2DStructuralLearning
    PoonAndDomingosStructuralLearning
    Examples
    --------
    **Creating a StructuralLearning object:**
    Create a StructuralLearning object and set its graph (it starts with a "null graph")
    >>> G = nx.DiGraph()
    >>> G.add_node(0,type='sum',label='+')
    >>> G.add_node(1,type='product',label='*')
    >>> G.add_node(2,type='leaf',scope=(0,0),label='(0,0)')
    >>> G.add_edges_from([(0,1),(1,2)])
    >>> learner = StructuralLearning()
    >>> learner.graph = G#don't call function get_spn without setting a graph
    """
    def __init__(self, input_shape, graph=None):
        """Initialize a StructuralLearning object with a null graph.
        Parameters
        ----------
        [nothing yet]
        See Also
        --------
        [nothing yet]
        Examples
        --------
        [nothing yet]
        """
        self.graph = graph
        # Input size: (Height, Width)
        self.input_shape = input_shape

    def get_spn(self):
        """This function builds the SPN using lilspn.node library given the object graph(Networkx).
        Parameters
        ----------
        [nothing yet]
        See Also
        --------
        get_structure
        learn_network
        Examples
        --------
        >>> G = nx.DiGraph()
        >>> G.add_node(0,type='sum',label='+')
        >>> G.add_node(1,type='product',label='*')
        >>> G.add_node(2,type='leaf',scope=(0,0),label='(0,0)')
        >>> G.add_edges_from([(0,1),(1,2)])
        >>> learner = StructuralLearning()
        >>> learner.graph = G#don't call function get_spn without setting a graph
        >>> spn = learner.get_spn()
        """
        #timers = TicTac()
        #timers.tic()
        #timers.tic()
        graph = self.graph if self.graph else self.get_structure()

        #dict to store all lil_spn objects(id:Node)
        nodes_dict = {}

        root_id = [n for n in graph.nodes() if graph.in_degree(n) == 0]
        
        nodes = list(nx.topological_sort(graph))
        nodes.reverse()
        counter=0
        len_nodes = len(nodes)
        for node_id in nodes:
            counter+=1
            #print('processing nodes: {}/{}'.format(counter,len_nodes))
            if graph.nodes[node_id]['type'] == 'sum':
                nodes_dict[node_id] = SumNode(*[nodes_dict[child_id] for child_id in graph.successors(node_id)])
            elif graph.nodes[node_id]['type'] == 'product':
                nodes_dict[node_id] = ProductNode(*[nodes_dict[child_id] for child_id in graph.successors(node_id)])
            elif graph.out_degree(node_id) == 0:
                #TODO better deal with specific leaf types
                if "leaf_class" in graph.nodes[node_id] and graph.nodes[node_id]["leaf_class"] == "GaussianLeafNode":
                    nodes_dict[node_id] = GaussianLeafNode((graph.nodes[node_id]['scope'][0],graph.nodes[node_id]['scope'][1]),-1,-1)
                else:
                    nodes_dict[node_id] = LeafNode((graph.nodes[node_id]['scope'][0],graph.nodes[node_id]['scope'][1]))
            nodes_dict[node_id].id = node_id
        # print('creating nodes: ',timers.toc())
        # timers.tic()
        spn = SumProductNetwork(nodes_dict[root_id[0]],self.input_shape, _nodes_dict=nodes_dict, _graph=graph)
        # print('creating instance: ',timers.toc())
        # print('internal get_spn: ',timers.toc())
        return spn

    def get_structure(self):
        """This returns the Networx graph which represents the SPN
        Parameters
        ----------
        [nothing yet]
        Returns
        -------
        graph : Networkx DiGraph object
            The Networkx object which represents the SPN graph
        See Also
        --------
        get_spn
        learn_network
        Examples
        --------
        [nothing yet]
        """
        self.graph = self.learn_network()
        return self.graph

    def learn_network(self):
        """This function generates the SPN graph(Networkx DiGraph object) given the structure learning  method
        Parameters
        ----------
        [nothing yet]
        Returns
        -------
        graph : Networkx DiGraph object
            The Networkx object which represents the SPN graph
        See Also
        --------
        get_structure
        get_spn
        Examples
        --------
        [nothing yet]
        """
        raise NotImplementedError




class Conv2DStructuralLearning(StructuralLearning):


    def __init__(self, input_shape, leaf_channels, sum_channels=2, pool_size=(2,2), ch_mode="constant", ch_pool_sequence=[]):
        super().__init__(input_shape)
        # Amount of leaf nodes
        self.leaf_channels = leaf_channels
        # Sequence of channels and pooling, starting with channels
        # Format: [(ch,(pool_h,pool_w)), ...]
        if len(ch_pool_sequence) > 0:
            self.ch_pool_sequence = ch_pool_sequence
        else:
            self.ch_pool_sequence = self._generate_ch_pool_sequences(self.input_shape,pool_size=pool_size,sum_channels=sum_channels,ch_mode=ch_mode)


    def learn_network(self):

        G = nx.DiGraph()
        
        # Preparing leaf nodes
        tot_num_leaves = self.input_shape[0]*self.input_shape[1]*self.leaf_channels
        leaves = np.reshape(np.array(range(tot_num_leaves)),(self.input_shape[0],self.input_shape[1],self.leaf_channels))

        # Add leaves to graph
        for h in range(leaves.shape[0]):
            for w in range(leaves.shape[1]):
                for c in range(leaves.shape[2]):
                    G.add_node(leaves[h,w,c],type="leaf",scope=(h,w),label="({},{})".format(h,w))

        # Global nodes id
        # Use dict so can pass as argument (by ref) and update inside functions
        node_id = {"value": tot_num_leaves}

        # Build graph
        next_layer = leaves
        for ch, pool_size in self.ch_pool_sequence:
            next_layer = self._single_2D_conv(next_layer, ch, G, node_id)
            next_layer = self._2d_pooling(next_layer, pool_size, G, node_id)
        # Root node
        next_layer = self._single_2D_conv(next_layer, 1, G, node_id)

        return G


    def _single_2D_conv(self, input_layer, out_channels, G, node_id):
        # Performs 2D convolution with single (1 x 1) sliding window

        output_layer = np.zeros((input_layer.shape[0],input_layer.shape[1],out_channels))

        for c in range(out_channels):
            for h in range(input_layer.shape[0]):
                for w in range(input_layer.shape[1]):
                    # Create sum node
                    node_id["value"] += 1
                    G.add_node(node_id["value"],type="sum", label="+")
                    for child_id in input_layer[h,w,:]:
                        G.add_edge(node_id["value"],int(child_id))
                    # Save node in output
                    output_layer[h,w,c] = node_id["value"]

        return output_layer


    def _2d_pooling(self, input_layer, win_size, G, node_id):
        # Performs pooling layer
        pad_size = self._compute_pad_size(input_layer.shape,win_size)
        output_size = self._compute_pooling_output_size(input_layer.shape,win_size,pad_size)

        output_layer = np.zeros((output_size[0],output_size[1],input_layer.shape[2]))

        input_layer_padded = np.pad(input_layer,((0,pad_size[0]),(0,pad_size[1]),(0,0)),"constant", constant_values=-1)

        for c in range(input_layer_padded.shape[2]):
            for h in range(output_layer.shape[0]):
                for w in range(output_layer.shape[1]):
                    # Create product node
                    node_id["value"] += 1
                    G.add_node(node_id["value"],type="product",label="*")
                    w_start_win = w * win_size[1]
                    w_stop_win = (w + 1) * win_size[1]
                    h_start_win = h * win_size[0]
                    h_stop_win = (h + 1) * win_size[0]
                    for child_id in input_layer_padded[h_start_win:h_stop_win,w_start_win:w_stop_win,c].flatten():
                        if child_id != -1:
                            G.add_edge(node_id["value"],int(child_id))
                    # Save node in output
                    output_layer[h,w,c] = node_id["value"]
        return output_layer


    def _compute_pad_size(self,input_shape,pool_size):

        h_pad = pool_size[0] - input_shape[0] % pool_size[0] if (input_shape[0] % pool_size[0] != 0) else 0
        w_pad = pool_size[1] - input_shape[1] % pool_size[1] if (input_shape[1] % pool_size[1] != 0) else 0

        return (h_pad,w_pad)


    def _compute_pooling_output_size(self,input_shape, pool_size, pad_size):

        h_pad, w_pad = pad_size

        h_output_size = int((input_shape[0] + h_pad) /  pool_size[0])
        w_output_size = int((input_shape[1] + w_pad) / pool_size[1])

        return (h_output_size,w_output_size)


    def _generate_ch_pool_sequences(self,input_shape,pool_size=(2,2),sum_channels=2,ch_mode="constant"):
        # Channel modes
        # - constant: all layers with the same amount of channels
        # - double: the next layer has the double of channels from the previous
        sequence = []
        current_input_shape = input_shape
        current_out_channels = sum_channels
        if ch_mode == "double":
            current_out_channels = int(sum_channels/2)
        while True:
            pad_size = self._compute_pad_size(current_input_shape,pool_size)
            current_input_shape = self._compute_pooling_output_size(current_input_shape,pool_size,pad_size)
            if ch_mode == "double":
                current_out_channels = 2 * current_out_channels
            sequence.append((current_out_channels,pool_size))
            if current_input_shape[0] < pool_size[0] and current_input_shape[1] < pool_size[1]:
                break
        return sequence




#(Pedro Domingos, 2011)
class PoonAndDomingosStructuralLearning(StructuralLearning):
    """
    This class inherits from StructuralLearning: the structure learning method described in (Domingos, 2011).
    Parameters
    ----------
    [nothing yet]
    See Also
    --------
    StructuralLearning
    Conv2DStructuralLearning
    Examples
    --------
    **Creating SPN using (Domingos, 2011) structure learning method**
    >>> learner = PoonAndDomingosStructuralLearning(input_size=(1,2),leaf_channels=1)
    >>> spn = learner.get_spn()#functions calls learn_network
    """
    def __init__(self, input_shape, leaf_channels=1, sum_node_channels=1, resolution_size=(1,1), type_cuts_selection='all'):
    #def __init__(self, input_shape, leaf_channels=1, sum_node_channels=1, resolution_size=(1,1), type_cuts_selection='all'):
        """Initialize a PoonAndDomingosStructuralLearning object.
        Parameters
        ----------
        input_shape : a tuple (Height, Width) with the Height and Width of the input 
        leaf_channels : a int for the amount of leaf nodes(or sum nodes per rectangle)[this must change to a list]
        resolution_size : a tuple (k_m,k_n) used as the resolution of the cuts
        See Also
        --------
        [nothing yet]
        Examples
        --------
        [nothing yet]
        """
        super().__init__(input_shape)
        # Amount of leaf nodes(or sum nodes per rectangle)
        self.leaf_channels = leaf_channels
        self.sum_node_channels = sum_node_channels
        # coarse resolution of decompositions: minimum of big rectangles and maximum of rectangle's size to start fine decompositions
        self.resolution_size = resolution_size
        self.type_cuts_selection = type_cuts_selection
        Region.reset_regions_dict()#to avoid interference of another cases using the class Region
        
    def learn_network(self):
        """This function builds the sum-product network (SPN) structure graph as described in (Pedro Domingos, 2011)
        Parameters
        ----------
        [nothing yet]
        Returns
        -------
        graph : Networkx DiGraph object
            The Networkx object which represents the SPN graph
        See Also
        --------
        get_structure
        get_spn
        Examples
        --------
        [nothing yet]
        """
        n_sum_leaf = self.leaf_channels
        n_sum = self.sum_node_channels 
        
        g = nx.DiGraph()
        global_nodes_id = [0] #identifier for all the SPN nodes

        __coarseRowStep = int(self.input_shape[0] / self.resolution_size[0])#ih/k_h
        __coarseColStep = int(self.input_shape[1] / self.resolution_size[1])#iw/k_w
        # This is used to remember all the segmented region id in order to avoid those clunky for-loop in other functions
        __coarseRegionId = []
        __fineRegionId = [] 
        __pixelRegionId = []

        # for notation simplicity
        iw = self.input_shape[1]
        ih = self.input_shape[0] 
        k_h = self.resolution_size[0]
        k_w = self.resolution_size[1]
        # for coarse regions
        #print('...... Creating coarse regions')
        for rowStepSize in range(1, __coarseRowStep + 1):
            for colStepSize in range(1, __coarseColStep + 1):            
                # skip the k_h x k_w pixels patches, they are fine regions(leaves of coarse regions tree)
                if colStepSize == 1 and rowStepSize == 1:
                    continue
                # find the range of a patch: l = left, r = right, d = down, u = up
                for l in range(0, iw - colStepSize * k_w + 1, k_w):
                    r = l + colStepSize * k_w
                    for u in range(0, ih - rowStepSize * k_h + 1, k_h):
                        d = u + rowStepSize * k_h
                        
                        num_id = Region.getRegionId(u, d, l, r, iw, ih, k_w, k_h)
                        region = Region.getRegion(num_id)
                        __coarseRegionId.append(num_id)
                        region.from_combination = (rowStepSize,colStepSize)
                        #print('coarse region(id:{}) from comb=({},{}):'.format(num_id,rowStepSize,colStepSize),'l:{} r:{} u:{} d:{}'.format(l,r,u,d))
                        if colStepSize == __coarseColStep \
                           and rowStepSize == __coarseRowStep:
                            # this is the root region
                            region.sumNodes = [global_nodes_id[0]]
                            #print('root region(id:{}) created with its sum node id: {}'.format(num_id,region.sumNodes))
                            global_nodes_id[0] +=1
                            __rootRegion = region
                        else:
                            region.sumNodes = list(range(global_nodes_id[0],(global_nodes_id[0]+n_sum)))
                            #print('coarse region(id:{}) created with its nodes id: {}'.format(num_id,region.sumNodes))
                            global_nodes_id[0] +=n_sum
        # for fine regions
        #print('...... Creating fine regions')
        for colStepSize in range(0, __coarseColStep):
            for rowStepSize in range(0, __coarseRowStep):
                for pixelColStep in range(1, k_w + 1):
                    for pixelRowStep in range(1, k_h + 1):
                        for l in range(colStepSize * k_w, (colStepSize + 1) * k_w - pixelColStep + 1):
                            r = l + pixelColStep
                            for u in range(rowStepSize * k_h, (rowStepSize + 1) * k_h - pixelRowStep + 1):
                                d = u + pixelRowStep
                                
                                num_id = Region.getRegionId(u, d, l, r, iw, ih, k_w, k_h)
                                region = Region.getRegion(num_id)
                                region.from_combination = (rowStepSize,colStepSize)
                                #print('fine region(id:{}) from leave/root comb=({},{}):'.format(num_id,rowStepSize,colStepSize),'l:{} r:{} u:{} d:{}'.format(l,r,u,d))
                                if pixelRowStep == 1 and pixelColStep == 1:
                                    #----------------- The region is a group of n_sum_leaf LEAF NODES(same scope)
                                    region.sumNodes = list(range(global_nodes_id[0],(global_nodes_id[0]+n_sum_leaf)))
                                    #print('leaf region(id:{}) scope({},{}) created with its node_id: {}'.format(num_id,region.rowUp,region.columnLeft,region.sumNodes))                              
                                    global_nodes_id[0] +=n_sum_leaf
                                    #adding the amount of nodes per leaf
                                    g.add_nodes_from(region.sumNodes,type='leaf',scope=(region.rowUp,region.columnLeft),label="leaf_regionId:{}:({},{})".format(region.id,region.rowUp,region.columnLeft))
                                else:
                                    if pixelColStep == k_w and pixelRowStep == k_h and __coarseColStep==1 and __coarseRowStep==1:
                                        # this is the root region when the resolution is the same as the image size
                                        __fineRegionId.append(num_id)
                                        region.sumNodes = [global_nodes_id[0]]
                                        #print('root region(id:{}) created with its sum node id: {}'.format(num_id,region.sumNodes))
                                        global_nodes_id[0] +=1
                                        __rootRegion = region
                                    else:
                                        #----------------- The region is a group of n_sum SUM NODES
                                        __fineRegionId.append(num_id)
                                        region.sumNodes = list(range(global_nodes_id[0],(global_nodes_id[0]+n_sum)))
                                        #print('fine region(id:{}) created with its nodes id: {}'.format(num_id,region.sumNodes))
                                        global_nodes_id[0] +=n_sum

        # Cutting regions(creating the n_prod product nodes per cut) and creating the edges between nodes
        #print('...... Starting with fine regions')
        counter = 1
        n_r = len(__fineRegionId)
        for region_id in __fineRegionId:
            region = Region.getRegion(region_id)
            #print('processing __fineRegionId: {}   counter: {}/{}'.format(region_id,counter,n_r))
            region.cut_region(g,global_nodes_id,self.type_cuts_selection)
            counter+=1
        #print('...... Starting with coarse regions')
        counter = 1
        n_r = len(__coarseRegionId)
        for region_id in __coarseRegionId:
            #print('processing __coarseRegionId: {}       counter: {}/{}'.format(region_id,counter,n_r))
            region = Region.getRegion(region_id)
            region.cut_region(g,global_nodes_id,self.type_cuts_selection)
            counter+=1
        
        #Deleting sum nodes at graph which doesn't have parents(except the root)
        root_id = __rootRegion.sumNodes[0]
        current_no_parent_nodes = [n for n in g.nodes() if g.in_degree(n) == 0 and n != root_id]
        #this should not be done unless one type of cuts selection was chosen.
        if self.type_cuts_selection == 'all' and len(current_no_parent_nodes) != 0:
            raise ValueError('Graph has more than the root node as a in_degree == 0')
        while current_no_parent_nodes:
            g.remove_nodes_from(current_no_parent_nodes)
            current_no_parent_nodes = [n for n in g.nodes() if g.in_degree(n) == 0 and n != root_id]
            
        return g




# -------------------
# Leaf Learning
# -------------------

class LeafLearning:


    def __init__(self, spn, data, n_components):

        rank = len(data.shape)
        if rank != 3:
            raise ValueError("Only 2D data accepted: shape of data must be of length 2 and format (batch,m,n)")

        self.input_rank = rank
        self.spn = spn
        self.data = data
        self.n_components = n_components


    def fit(self):

        leaf_mapping = self.fit_and_replace()

        if leaf_mapping is None:
            raise ValueError("Missing node replacement map")

        for old_leaf_id in leaf_mapping:
            self.spn.nodes_dict[old_leaf_id] = leaf_mapping[old_leaf_id]

        # Recompute internal spn state
        self.spn.setup()


    def fit_and_replace(self):
        raise NotImplementedError





class PercentileGaussianLeafLearning(LeafLearning):


    @staticmethod
    def build_gaussian_leaf_node(leaf_node, mean, std):
        g_leaf = GaussianLeafNode(leaf_node.scope,mean,std)
        g_leaf.leaf_input = leaf_node.leaf_input
        g_leaf.id = leaf_node.id
        return g_leaf


    def fit_and_replace(self):

        # Find mean and standard deviation
        self.means = np.zeros((self.data.shape[1], self.data.shape[2], self.n_components))
        self.stds = np.zeros((self.data.shape[1], self.data.shape[2], self.n_components))
        sorted_data = np.sort(self.data, axis=0)
        quantile_size = self.data.shape[0] / self.n_components
        for k in range(self.n_components):
            lower_idx = int(k * quantile_size)
            upper_idx = int((k + 1) * quantile_size)
            self.means[:,:,k] = np.mean(sorted_data[lower_idx:upper_idx, :, :], axis=0)
            self.stds[:,:,k] = np.std(sorted_data[lower_idx:upper_idx, :, :], axis=0)

        # Replace leaf nodes from SPN with Gaussian ones
        leaf_mapping = {}
        # Although the generator does not iterate through a node multiple times,
        # we iterate through the children of each node here, which might
        # pass through the same leaf node more than once
        gauss_leaf_nodes = {}
        leaf_node_counter = {}
        for node in self.spn.topological_sort():
            for i,child in enumerate(node.children):
                if isinstance(child,LeafNode):
                    if child in gauss_leaf_nodes:
                        # update parent reference to new leaf
                        node.children[i] = gauss_leaf_nodes[child]
                    else:
                        # create new leaf
                        if not child.scope in leaf_node_counter:
                            leaf_node_counter[child.scope] = 0
                        else:
                            leaf_node_counter[child.scope] += 1
                        # use mean and std
                        mean = self.means[child.scope[0],child.scope[1],leaf_node_counter[child.scope]]
                        std = self.stds[child.scope[0],child.scope[1],leaf_node_counter[child.scope]]
                        new_leaf = PercentileGaussianLeafLearning.build_gaussian_leaf_node(child,mean,std)
                        gauss_leaf_nodes[child] = new_leaf
                        # update parent reference to new leaf
                        node.children[i] = new_leaf
                        # Save old/new mapping
                        leaf_mapping[child.id] = new_leaf

        return leaf_mapping
