from distutils.core import setup

setup(name='lilspn',
      version='0.1',
      description='Little SPN Library',
      author='Jhonatan S. Oliveira, Andre E. dos Santos, Andre L. Teixeira',
      author_email='jhonatanoliveira@gmail.com, andre.eds@gmail.com, andreloboteixeira@gmail.com',
      packages=['lilspn'],
      py_modules=['numpy', 'tensorflow', 'networkx']
     )